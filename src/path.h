/// Minimal class to represent a path
/// where all nodes are represented by ints
/// so that operations are fast.
///
#ifndef CHROMOTRACE_PATH_H
#define CHROMOTRACE_PATH_H

#include <list>
#include <algorithm>
class path {

public:
    path(): num_probes(0) {}

    /// typedef for the node iterator to made code a bit cleaner
    typedef std::list<unsigned long>::const_iterator const_node_iterator;

    /// Get the first node in the path. Note: The notion of 'first' and 'last' don't have any meaning
    /// \return the first node in the path
    unsigned long  get_first_node()     const {return nodes_path.front();}
    /// Get the last node in the path. Note: The notion of 'first' and 'last' don't have any meaning
    /// \return the last node in the path
    unsigned long  get_last_node()      const {return nodes_path.back();}
    /// Get the size of the path
    /// \return the number of nodes in the path
    unsigned long  get_size()           const {return nodes_path.size();}

    /// Return the front iterator to the path
    /// \return the front iterator to the path
    const_node_iterator get_front_iterator() const {return nodes_path.begin();}
    /// Return the back iterator to the path
    /// \return the back iterator to the path
    const_node_iterator get_end_iterator()   const {return nodes_path.end();}

    /// Add a node to the front of the path
    /// \param new_front node to add
    void add_first_node(const unsigned long new_front) {nodes_path.push_front(new_front);}
    /// Add a node to the back of the path
    /// \param new_back node to add
    void add_last_node (const unsigned long new_back)  {nodes_path.push_back(new_back);}
    /// Remove first node in list
    void remove_first_node() {nodes_path.pop_front();}
    /// Remove last node in list
    void remove_last_node () {nodes_path.pop_back();}
    /// Check if the supplied item is in the current path
    /// \param item the node to search for in the path
    /// \return true if the item is in the path and false otherwise
    bool in_path(const unsigned long item){return (nodes_path.end() != std::find (nodes_path.begin(), nodes_path.end(), item));}

    /// Get the number of unqiue nodes in the path
    /// \return the number of unique nodes in the path
    unsigned long unique_node_numbers() {std::list<unsigned long> temp(nodes_path); temp.sort();temp.unique();
        return temp.size();}//This is a terrible method
    /// Override the < operator to compare the size of lists
    /// \param left path to compare if the right is less than left path
    /// \param right path to compare if bigger than right path
    friend bool operator<(const path& left, const path& right){
        if(left.get_size()<right.get_size())
            return true;
        else if (left.get_size()>right.get_size())
            return false;
        else
            return left.nodes_path < right.nodes_path;
    }
    /// Override the > operator to compare the size of lists
    /// \param left path to compare if the left is bigger than the right path
    /// \param right path to compare if the left is bigger than the right path
    friend bool operator>(const path& left, const path& right){
        if(left.get_size()>right.get_size())
            return true;
        else if (left.get_size()<right.get_size())
            return false;
        else
            return left.nodes_path > right.nodes_path;
    }

private:
    unsigned long            num_probes;
    std::list<unsigned long> nodes_path;
};


#endif //CHROMOTRACE_PATH_H
