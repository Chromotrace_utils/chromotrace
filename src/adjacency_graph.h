
/// New adjacency graph class which provides
/// functionality to build the matrix and
/// expose the required functions for the rest
/// of the algorithm.
///
///
/// Note: that this class has been heavily stripped down. Methods that require
/// combining information from the adjacency graph and suffix tree should not
/// be put here.

#ifndef CHROMOTRACE_NEW_ADJACENCY_GRAPH_H
#define CHROMOTRACE_NEW_ADJACENCY_GRAPH_H

#include <list>
#include <iostream>
#include <algorithm>
#include <vector>


class adjacency_graph {//TODO: define destructor

public:

     adjacency_graph() {
         min_degree = 0;
         max_degree = 0;

         num_seg_probes = 0;
         initial_num_seg_probes = 0;
     };

    /// Constructor to build the adjacency graph class from a parameter object
    /// \param params is a parameter object used to initialise the adjacency graph.
    adjacency_graph(const std::vector<long double> &x_vec, const std::vector<long double> &y_vec, const std::vector<long double> &z_vec, float threshold) {

        if((x_vec.size()!=y_vec.size() || (y_vec.size()!=z_vec.size()))){
            std::cerr << "Vector sizes do not match, cannot create graph" << std::endl;
            exit(1);
        }

        std::cout << "Building Adjacency Graph" << std::endl;
        num_seg_probes         = x_vec.size();
        initial_num_seg_probes = x_vec.size();

        std::cout << "START: " <<  num_seg_probes << " Points Attempting to Load, " << std::endl;

        build_distance_matrix(x_vec,y_vec,z_vec,threshold);

        if(num_seg_probes!=0){
            min_degree = *std::min_element(degree_array.begin(), degree_array.end());
            max_degree = *std::max_element(degree_array.begin(), degree_array.end());
        } else {
            min_degree = 0;
            max_degree = 0;
        }

        if(max_degree==0){
            std::cout << " All nodes have a 0 degree. Increase the 'Physical Distance Threshold'" << std::endl;
            std::cout << "-------- END COMPUTATION --------" << std::endl;
        }

        std::cout << "DONE: " <<  num_seg_probes << " Points Correctly Loaded, " << std::endl;
        std::cout << "Min Node Degree =" << min_degree << ", " << std::endl;
        std::cout << "Max Node Degree =" << max_degree << std::endl;
    }

    unsigned long getInitialNumSegProbes() const;

    void  remove_node(unsigned int node_index);

    unsigned long get_num_seg_probes() const;
    unsigned int  get_degree (unsigned int node_index)     const;
    //bool get_element(unsigned int x, unsigned int y) const;

    std::list<unsigned long>::const_iterator get_adj_nodes_front(unsigned int node_index) const;
    std::list<unsigned long>::const_iterator get_adj_nodes_back (unsigned int node_index) const;
    std::list<unsigned long> get_degree_adj_nodes(unsigned int node_index) const;

    int getMinDegree() const;
    int getMaxDegree() const;

private:

    class Point{
    public:
        const long double x;
        const long double y;
        const long double z;

        Point(const long double x, const long double y, const long double z): x(x), y(y), z(z){}
    };

    //std::vector<bool,bool> distance_matrix;// Data structure for the distance matrix
    std::vector<unsigned int>  degree_array;//For quick access to degree of node
    std::vector<unsigned long> order_indexes;
    std::vector<unsigned long> order_indexes_inv;

    std::vector<std::list<unsigned long>> adj_nodes;//For quick access to adjacent nodes

    /**START: Information about seg probes. These are legacy vairables but might be useful**/
    int min_degree;
    int max_degree;
    unsigned long num_seg_probes, initial_num_seg_probes;
    /**END**/

    void  build_distance_matrix(std::vector<long double> x, std::vector<long double> y, std::vector<long double> z, float threshold);

    float compute_euclidean_distance(const Point&& a, const Point&& b) const;
    float compute_euclidean_distance(const Point&  a, const Point&  b) const;

    float compute_euclidean_distance_no_sqrt(const Point&& a, const Point&& b) const;
    float compute_euclidean_distance_no_sqrt(const Point&  a, const Point&  b) const;

    float compute_manhatten_distance(const Point&& a, const Point&& b) const;
    float compute_manhatten_distance(const Point&  a, const Point&  b) const;
};


#endif //CHROMOTRACE_NEW_ADJACENCY_GRAPH_H
