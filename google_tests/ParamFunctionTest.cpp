//
// Created by Odyss on 10/03/2020.
//

#include <gtest/gtest.h>
#include "../src/parameters.h"
#include <gmock/gmock.h>

class ParamFunctionTest: public ::testing::Test {
public:
    ParamFunctionTest( ) {
        // initialization;
        // can also be done in SetUp()

    }

    parameters test;
    parameters test2;
    parameters test3;
    parameters test4;
    parameters test5;

    ///
    /// File paths are hardcoded. All files referenced below are in the test_data/unit_test directory
    ///
    void SetUp( ) {
        std::string configfile("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_data.conf");

        ///Use string pass by address constructor
        test = parameters(configfile);

        ///Use string pass by r-value constructor
        test2 = parameters("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_perm1_data.conf");

        ///Use string pass by r-value constructor
        test3 = parameters("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_perm2_data.conf");

        ///Use string pass by r-value constructor
        test4 = parameters("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_perm3_data.conf");

        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~ParamFunctionTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};

TEST_F(ParamFunctionTest, GetXVecTest) {
    std::vector<unsigned long> exp_x_vec = {26,26,27,27,27,27,27,26,26};

    auto cur_x_vec = test.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), cur_x_vec.at(count));
    }

    exp_x_vec = {27,27,26,27,27,26,26,26,27};

    cur_x_vec = test2.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), cur_x_vec.at(count));
    }

    exp_x_vec = {27,26,27,26,27,26,27,26,27};

    cur_x_vec = test3.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), cur_x_vec.at(count));
    }

    exp_x_vec = {26,26,27,27,27,26,26,27,27};

    cur_x_vec = test4.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), cur_x_vec.at(count));
    }
}

TEST_F(ParamFunctionTest, GetYVecTest) {
    std::vector<unsigned long> exp_y_vec = {87,86,86,86,85,85,84,84,85};

    auto cur_y_vec = test.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), cur_y_vec.at(count));
    }

    exp_y_vec = {85,85,87,84,86,86,85,84,86};

    cur_y_vec = test2.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), cur_y_vec.at(count));
    }

    exp_y_vec = {85,85,86,84,84,87,86,86,85};

    cur_y_vec = test3.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), cur_y_vec.at(count));
    }

    exp_y_vec = {85,86,85,85,86,84,87,86,84};

    cur_y_vec = test4.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), cur_y_vec.at(count));
    }
}

TEST_F(ParamFunctionTest, GetZVecTest) {
    std::vector<unsigned long> exp_z_vec = {56,56,56,57,57,58,58,58,58};

    auto cur_z_vec = test.get_z_vec();

    for(auto count=0; count<cur_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), cur_z_vec.at(count));
    }

    exp_z_vec = {57,58,56,58,56,56,58,58,57};

    cur_z_vec = test2.get_z_vec();

    for(auto count=0; count<cur_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), cur_z_vec.at(count));
    }

    exp_z_vec = {58,58,56,58,58,56,57,56,57};

    cur_z_vec = test3.get_z_vec();

    for(auto count=0; count<cur_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), cur_z_vec.at(count));
    }

    exp_z_vec = {58,56,58,57,56,58,56,57,58};

    cur_z_vec = test4.get_z_vec();

    for(auto count=0; count<cur_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), cur_z_vec.at(count));
    }
}

TEST_F(ParamFunctionTest, GetXTest) {
    std::vector<unsigned long> exp_x_vec = {26,26,27,27,27,27,27,26,26};

    auto cur_x_vec = test.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), test.get_x(count));
    }

    exp_x_vec = {27,27,26,27,27,26,26,26,27};

    cur_x_vec = test2.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), test2.get_x(count));
    }

    exp_x_vec = {27,26,27,26,27,26,27,26,27};

    cur_x_vec = test3.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), test3.get_x(count));
    }

    exp_x_vec = {26,26,27,27,27,26,26,27,27};

    cur_x_vec = test4.get_x_vec();

    for(auto count=0; count<cur_x_vec.size(); ++count){
        ASSERT_EQ(exp_x_vec.at(count), test4.get_x(count));
    }
}

TEST_F(ParamFunctionTest, GetYTest) {
    std::vector<unsigned long> exp_y_vec = {87,86,86,86,85,85,84,84,85};

    auto cur_y_vec = test.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), test.get_y(count));
    }

    exp_y_vec = {85,85,87,84,86,86,85,84,86};

    cur_y_vec = test2.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), test2.get_y(count));
    }

    exp_y_vec = {85,85,86,84,84,87,86,86,85};

    cur_y_vec = test3.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), test3.get_y(count));
    }

    exp_y_vec = {85,86,85,85,86,84,87,86,84};

    cur_y_vec = test4.get_y_vec();

    for(auto count=0; count<cur_y_vec.size(); ++count){
        ASSERT_EQ(exp_y_vec.at(count), test4.get_y(count));
    }
}

TEST_F(ParamFunctionTest, GetZTest) {
    std::vector<unsigned long> exp_z_vec = {56,56,56,57,57,58,58,58,58};

    for(auto count=0; count<exp_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), test.get_z(count));
    }

    exp_z_vec = {57,58,56,58,56,56,58,58,57};

    for(auto count=0; count<exp_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), test2.get_z(count));
    }

    exp_z_vec = {58,58,56,58,58,56,57,56,57};

    for(auto count=0; count<exp_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), test3.get_z(count));
    }

    exp_z_vec = {58,56,58,57,56,58,56,57,58};

    for(auto count=0; count<exp_z_vec.size(); ++count){
        ASSERT_EQ(exp_z_vec.at(count), test4.get_z(count));
    }
}

TEST_F(ParamFunctionTest, GetColTest) {
    std::vector<char> exp_col = {'e','c','j','a','i','f','g','b','e'};

    for(auto count=0; count<exp_col.size(); ++count){
        ASSERT_EQ(exp_col.at(count), test.get_colour(count));
    }

    exp_col = {'i','f','e','g','j','c','e','b','a'};

    for(auto count=0; count<exp_col.size(); ++count){
        ASSERT_EQ(exp_col.at(count), test2.get_colour(count));
    }

    exp_col = {'f','e','j','b','g','e','a','c','i'};

    for(auto count=0; count<exp_col.size(); ++count){
        ASSERT_EQ(exp_col.at(count), test3.get_colour(count));
    }

    exp_col = {'e','c','f','i','j','b','e','a','g'};

    for(auto count=0; count<exp_col.size(); ++count){
        ASSERT_EQ(exp_col.at(count), test4.get_colour(count));
    }
}

TEST_F(ParamFunctionTest, GetThresholdTest) {
    ASSERT_EQ(test.get_threshold(),1.0);
    ASSERT_EQ(test2.get_threshold(),1.5);

    ///Specified as 5 in the config
    ASSERT_EQ(test3.get_threshold(),5.000);
    ASSERT_EQ(test3.get_threshold(),5.0);
    ASSERT_EQ(test3.get_threshold(),5);

    ///Specified as 5.0 in the config file
    ASSERT_EQ(test4.get_threshold(),5.000);
    ASSERT_EQ(test4.get_threshold(),5.0);
    ASSERT_EQ(test4.get_threshold(),5);

}

TEST_F(ParamFunctionTest, GetLabelStringTest) {
    ASSERT_EQ(test.getLabel_string(),"ecjaifgbe");
    ASSERT_EQ(test2.getLabel_string(),"efegjacib");
    ASSERT_EQ(test3.getLabel_string(),"bjefegcai");
    ASSERT_EQ(test4.getLabel_string(),"fegcaebij");
}

TEST_F(ParamFunctionTest, GetLabelRevStringTest) {
    ASSERT_EQ(test.getLabel_string_reverse() ,"ebgfiajce");
    ASSERT_EQ(test2.getLabel_string_reverse(),"bicajgefe");
    ASSERT_EQ(test3.getLabel_string_reverse(),"iacgefejb");
    ASSERT_EQ(test4.getLabel_string_reverse(),"jibeacgef");
}

TEST_F(ParamFunctionTest, GetLabelSubStringTest) {
    ASSERT_EQ(test.getLabel_sub_string(0,4) ,"ecja");

    /// This length extends past the end of the string, up until the end of the string is returned.
    ASSERT_EQ(test2.getLabel_sub_string(3,7),"gjacib");

    /// This start of the substring is past the end of the string, nothing should be returned.
    ASSERT_EQ(test2.getLabel_sub_string(10,7),"");

    ASSERT_EQ(test3.getLabel_sub_string(0,9),"bjefegcai");
    ASSERT_EQ(test4.getLabel_sub_string(1,7),"egcaebi");
}

TEST_F(ParamFunctionTest, GetStartPosTest) {
    std::vector<unsigned long> exp_start_pos = {1,10801,21601,32401,43201,54001,64801,75601,86401};

    for(auto count=0; count<exp_start_pos.size(); ++count){
        ASSERT_EQ(exp_start_pos.at(count), test.get_start_pos(count));
    }

    exp_start_pos = {1,54001,86401,64801,21601,32401,10801,43201,75601};

    for(auto count=0; count<exp_start_pos.size(); ++count){
        ASSERT_EQ(exp_start_pos.at(count), test2.get_start_pos(count));
    }

    exp_start_pos = {75601,21601,86401,54001,1,64801,10801,32401,43201};

    for(auto count=0; count<exp_start_pos.size(); ++count){
        ASSERT_EQ(exp_start_pos.at(count), test3.get_start_pos(count));
    }

    exp_start_pos = {54001,1,64801,10801,32401,86401,75601,43201,21601};

    for(auto count=0; count<exp_start_pos.size(); ++count){
        ASSERT_EQ(exp_start_pos.at(count), test4.get_start_pos(count));
    }
}

TEST_F(ParamFunctionTest, GetEndPosTest) {

    std::vector<unsigned long> exp_end_pos = {10800,21600,32400,43200,54000,64800,75600,86400,97200};

    for(auto count=0; count<exp_end_pos.size(); ++count){
        ASSERT_EQ(exp_end_pos.at(count), test.get_end_pos(count));
    }

    exp_end_pos = {10800,64800,97200,75600,32400,43200,21600,54000,86400};

    for(auto count=0; count<exp_end_pos.size(); ++count){
        ASSERT_EQ(exp_end_pos.at(count), test2.get_end_pos(count));
    }

    exp_end_pos = {86400,32400,97200,64800,10800,75600,21600,43200,54000};

    for(auto count=0; count<exp_end_pos.size(); ++count){
        ASSERT_EQ(exp_end_pos.at(count), test3.get_end_pos(count));
    }

    exp_end_pos = {64800,10800,75600,21600,43200,97200,86400,54000,32400};

    for(auto count=0; count<exp_end_pos.size(); ++count){
        ASSERT_EQ(exp_end_pos.at(count), test4.get_end_pos(count));
    }
}

TEST_F(ParamFunctionTest, GetChromoTest) {
    std::vector<std::string> exp_chromo = {"1","1","1","1","1","1","1","1","1"};

    for(auto count=0; count<exp_chromo.size(); ++count){
        ASSERT_EQ(exp_chromo.at(count), test.get_chromo(count));
    }

    for(auto count=0; count<exp_chromo.size(); ++count){
        ASSERT_EQ(exp_chromo.at(count), test2.get_chromo(count));
    }

    for(auto count=0; count<exp_chromo.size(); ++count){
        ASSERT_EQ(exp_chromo.at(count), test3.get_chromo(count));
    }

    for(auto count=0; count<exp_chromo.size(); ++count){
        ASSERT_EQ(exp_chromo.at(count), test4.get_chromo(count));
    }
}

TEST_F(ParamFunctionTest, ReadParamsTest1) {
    std::string configfile("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_data.conf");
    test5.read_params(configfile);
    ASSERT_EQ(test5.get_threshold(),1.0);
}

TEST_F(ParamFunctionTest, ReadParamsTest2) {
    std::string configfile("/mnt/c/Users/Odyss/Documents/chromotrace/test_data/unit_test/unit_test1_data.conf");
    std::string output_loc("test/path");

    test5.read_params(configfile,output_loc,5);
    ASSERT_EQ(test5.get_threshold(),5.0);
    ASSERT_EQ(output_loc,"test/path");
}
