//
// Created by carl on 30/09/16.
//
#include <iostream>
#include "suffix_tree.h"

/// Return true if pos is a valid position.
/// \param position is the position to check if it is valid
bool suffix_tree::valid_pos(unsigned long position) const {
    return position>0 && position<get_text_length();
}

/// Method to return a specified suffix from the suffix tree up to a given length
/// \param suffix_number is a valid suffix of the text
/// \param length the length of the suffix to return
/// \return the suffix starting at suffix number
std::string suffix_tree::get_suffix(const unsigned long suffix_number, const unsigned long length){
    std::string remaining_suffix;

    assert(suffix_number<text_length);

    auto leaf  = get_suffix_leaf(suffix_number+1);
    auto limit = std::min<unsigned long>(cst.depth(leaf),length);

    for(auto counter=1; counter<=limit; counter++){
        remaining_suffix.push_back(cst.edge(leaf, counter));
    }

    return remaining_suffix;
}

/// Method to return a specified suffix from the suffix tree.
/// \param suffix_number is a valid suffix of the text
/// \return the suffix starting at suffix number
std::string suffix_tree::get_suffix(const unsigned long suffix_number){
    return get_suffix(suffix_number, text_length);
}

/// Compute the shortest string with 1 occ. This is a leaf with an edge label of length >1
///
/// \return the minimum depth of a unique string in the suffix tree
unsigned long suffix_tree::compute_unique_min_depth() const{
    auto minimum_depth = ULONG_MAX;

    for(auto leaf_index=1; leaf_index <=text_length;leaf_index++){
        node_type current_leaf = cst.select_leaf((size_type) leaf_index);//get a leaf node

        auto parent_depth = cst.depth(cst.parent(current_leaf));
        if(cst.depth(current_leaf)-parent_depth>1){//check if the edge label between lead and parent >1
            if(minimum_depth>(parent_depth+0))
                minimum_depth = (minimum_depth> parent_depth+1) ? parent_depth+1 : minimum_depth;
        }
    }

    std::cout << "Minimum Depth: " << minimum_depth<< std::endl;
    return minimum_depth;
}

/// Not sure this actually does what it is supossed to do
/// \param suffix is the number of the suffix leaf requested
/// \return the node representing the leaf for suffix specified
node_type suffix_tree::get_suffix_leaf(const unsigned long suffix) const {
    return cst.select_leaf(suffix);
}

/// Get the parent node of the specified node in the suffix tree
/// \param node the node we want the parent of
/// \return the parent of node
node_type suffix_tree::get_parent(const node_type node) const {
    return cst.parent(node);
}

/// Mark a node in the suffix tree
/// \param node the node we want to marl
void suffix_tree::mark_node(const node_type node) {

    if(marked[cst.id(node)]!=2){
        marked[cst.id(node)] += 1;
        unique[cst.id(node)] = marked[cst.id(node)]!=2;
        auto alphabet_size = cst.degree(cst.root());//Get alphabet size
        auto result = 0;
        auto parent = cst.parent(node);
        auto temp   = cst.root();

        for(auto alpha_count = 0; alpha_count< alphabet_size; ++alpha_count){
            temp = cst.edge(parent, alpha[alpha_count]);

            if(!is_root(temp)) {
                result += (marked[cst.id(temp)]==2);
            }
        }

        if(result==1) {
            unique[cst.id(parent)] = true;
            unique_node[cst.id(parent)] = unique_node[cst.id(node)];
        }
    }
}

/// Mark a leaf of the suffix tree.
/// \param leaf_num node the node we want to mark
void suffix_tree::mark_leaf(const unsigned long leaf_num) {

    auto node = cst.select_leaf(leaf_num);

    if(marked[cst.id(node)]!=2){
        marked[cst.id(node)] += 1;
        if((marked[cst.id(node)]==2)){
            unique[cst.id(node)] = false;
        }

        auto result = 0;
        auto parent = cst.parent(node);
        node_type temp;

        for(auto alpha_count = 0; alpha_count< alpha.size(); ++alpha_count){
            temp = cst.edge(parent, alpha[alpha_count]);
            if(!is_root(temp)) {
                result = result +  (unique[cst.id(temp)]);
            }
        }
        std::cout << "result " << result << " " << leaf_num << std::endl;
        if(result==1) {
            std::cout << "change unique" <<std::endl;
            unique[cst.id(parent)] = true;
            unique_node[cst.id(parent)] = unique_node[cst.id(node)];
        } else if(result==0) {

        }
    }
}

/// Determine if the given pattern occurs uniquely in the text with length less than depth.
/// \param pattern the pattern to search for
/// \param depth representing the maximum depth
/// \return return the leaf node representing the pattern location
/// if it is unique and root otherwise
node_type suffix_tree::is_unique(const std::string& pattern, const unsigned long depth) const {//Simulate truncated suffix trees

    unsigned long pattern_length = (pattern.length() < depth) ? pattern.length() : depth;

    assert(depth < text_length);//assert the depth is not longer than the text
    assert(depth > 0);

    node_type current_node  = cst.child(cst.root(), pattern.at(0));
    size_type current_depth = cst.depth(current_node);

    for(auto index_to_match = current_depth; index_to_match<pattern_length;index_to_match++){

        if(cst.edge(current_node,current_depth)!=pattern.at(index_to_match-current_depth)) {///True if we find a mismatch before reaching the end of the pattern.
            if (cst.size(current_node) == 1) {///Check if matched portion of pattern occurs uniquely
                return current_node;
            } else {///Matched portion of pattern does not occur uniquely. return root to indicate this.
                return cst.root();
            }
        }

        if(index_to_match==current_depth-1)
            current_node = cst.child(current_node,pattern[index_to_match+1]);
    }
    return current_node; ///If the pattern occurs exactly once return the end of the edge.
}


/// Take a pattern p of length m and compute the longest match
/// for each suffix of p as well as their start position in the text
/// if this match is unique
///
/// \param pattern the pattern to search for
/// \return std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> that
/// represents the matching_statistics vector and a vector representing their locations
std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> suffix_tree::unique_matching_statistics(std::string pattern) const {
    /// Def. msXY(i) := the longest substring of pattern that starts at i and matches someplace in the text.
    /// Algorithm sketch to compute msXY(i)
    ///
    /// Step 1: Compute msXY(1) by querying for X in TY.
    /// The depth of the node where you stop is msXY(1)
    ///
    /// Step 2 for 1<i<n
    /// Follow the suffix link from where the search stopped in the previous step/iteration.
    /// Continue searching for X where you left off in the pattern.
    /// msXY(i) = the depth where the search gets stuck

    node_type current_node  = cst.root();
    node_type next_node     = cst.child(current_node, pattern.at(0));

    auto current_depth    = 0;
    auto pattern_length   = pattern.length();
    auto next_match_stats = 0;

    ///
    /// vectors for storing the matching statistics and the location of the match in the text
    ///
    std::vector<unsigned long> matching_stats(pattern_length);
    std::vector<unsigned long> matching_location(pattern_length);

    for(auto init_counter=0;init_counter<pattern_length;init_counter++){matching_stats.at(init_counter) = 0;} ///Init the array with max possible values.

    pattern.append("?");
    auto pattern_index=0;
    while(next_match_stats<pattern_length) {
        assert(current_depth >= 0);
        if ((cst.depth(next_node) == current_depth) || is_root(next_node)){///We are matching at an explicit node
            if (is_root(next_node) && is_root(current_node)) {///current character of the pattern doesn't exist in the text at all, skip this character

                matching_stats.at(next_match_stats) = 0;
                matching_location.at(next_match_stats) = Ambiguous_Loc;
                next_match_stats++;
                pattern_index++;

                next_node = (pattern_index<=pattern_length) ? cst.child(current_node, pattern.at(pattern_index)) : cst.root();

            } else if (is_root(next_node)) {///current character of the pattern doesn't exist at this node

                matching_stats.at(next_match_stats) = current_depth;
                matching_location.at(next_match_stats) = (cst.is_leaf(current_node)) ? cst.sn(current_node): Ambiguous_Loc;
                next_match_stats++;

                current_node  = cst.sl(current_node);///As current character doesn't match we follow the suffix link.
                current_depth = cst.depth(current_node);
                next_node = (pattern_index<=pattern_length) ? cst.child(current_node, pattern.at(pattern_index)) : cst.root();
            } else {///There is a match, as we are currently at an explicit node we need to get the next node to continue matching

                current_node  = next_node;
                current_depth = cst.depth(current_node);
                next_node = (pattern_index<=pattern_length) ? cst.child(current_node, pattern.at(pattern_index)) : cst.root();

            }
        } else if (cst.edge(next_node, current_depth + 1) != pattern.at(pattern_index)) {///We are currently performing matching on on an edge and there is a mismatch.

            ///As there is a mismatch we update the matching statistics and location.
            matching_stats.at(next_match_stats)    = current_depth;
            matching_location.at(next_match_stats) = (cst.is_leaf(next_node)) ? cst.sn(next_node): Ambiguous_Loc;

            current_depth--;
            next_match_stats++;
            auto target_depth = current_depth;

            current_node  = cst.sl(current_node);///Follow the suffix link for the current node
            current_depth = cst.depth(current_node);
            assert(next_match_stats + current_depth);
            next_node     = (pattern_length>=next_match_stats + current_depth) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();

            while (target_depth - current_depth != 0) {
            ///
            /// This loop puts the pointer to the correct depth in the suffix tree after following the suffix link.
            /// After following the suffix link the current node will be too shallow in the suffix tree because we were not currently at an explicit node.
            /// If this is the case we know there is more of the pattern matched and can jump to this depth without repeating the matching process.
            /// This loop skips nodes until we find the edge we should be on, then sets the current_depth to the target_depth and
            ///
                if (target_depth - current_depth> 0) {///The next node has a depth less than the target depth. So we don't performing matching and set the current node to the next node
                    current_node  = next_node;
                    current_depth = cst.depth(current_node);
                    next_node     = (pattern_length>=next_match_stats + current_depth) ? cst.child(current_node, pattern.at(next_match_stats + current_depth)) : cst.root();
                } else if (target_depth - current_depth< 0) {///count to correct position on edge
                    next_node     = current_node;
                    current_node  = cst.parent(current_node);
                    current_depth = target_depth;
                }
            }
        } else {///We are matching on an edge and the characters being compared match.
            current_depth++;
            pattern_index++;
        }
    }

    return std::make_tuple(matching_stats,matching_location);
}

/// Determines if the paramater node is the root node
/// \param node valid node in the tree
/// \return true if it is a root and false otherwise
bool suffix_tree::is_root(node_type node) const {
    return cst.root()==node;
}

/// Get the depth of the shortest unique string in the suffix tree
/// \return return the minimum depth of a unique node in the suffix tree
unsigned long suffix_tree::get_min_depth() const {
    return min_depth;
}

/// Returns the root node of the suffix tree
/// \return the root node in the suffix tree
node_type suffix_tree::get_root() const{
    return cst.root();
}

/// Return the number of nodes in the suffix tree.
///
/// \return the number of nodes in the suffix tree
size_type suffix_tree::get_size() const{
    return cst.size();
}

/// Get text length
/// \return text length the length of the string
unsigned long suffix_tree::get_text_length() const{
    return text_length;
}
