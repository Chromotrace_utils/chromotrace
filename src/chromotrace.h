/// Main class of the algorithm that
/// manages the construction of all the
/// data structures and their use

#ifndef CHROMOTRACE_NEW_PATHS_H
#define CHROMOTRACE_NEW_PATHS_H

#include <iostream>
#include "path.h"
#include "adjacency_graph.h"
#include "suffix_tree.h"

class chromotrace {

public:
    ///Main contructor to build the chromomtrace object that manages the search and labelling of the adjacency graphs
    /// \param params is a pre build parameter object
    /// \param agraph is a prebuild adjacency graph object that is assumed to be built from the same parameter object that was passed
    /// \param stree is a suffix tree of the forward string stored in the parameter object
    /// \param rtree is a suffix tree of the reverse of the string stored in the parameter object
    chromotrace(const parameters &params, const adjacency_graph &agraph, const suffix_tree &stree, const suffix_tree &rtree) : params(params), agraph(agraph), stree(stree), rtree(rtree){

        assigned = std::vector<bool>(agraph.getInitialNumSegProbes());
        twice_assigned = std::vector<int>(stree.get_text_length());

        for(auto point = 0; point < stree.get_text_length(); ++point){twice_assigned[point] = 0;}
        for(auto node_index = 0; node_index < agraph.getInitialNumSegProbes(); node_index++){assigned[node_index]=false;}

        std::cout << "Start chromotrace "   << std::endl;
        std::cout << "start finding paths" << std::endl;
        next_string     = params.getLabel_string();
        next_string_rev = params.getLabel_string_reverse();

        find_paths();
    }

private:

    enum Direction: bool {//Enum to define pseudo directions for expansion of paths
        Forward  = false,
        Reverse  = true
    };

    enum Extend_Length: unsigned long {//Enum to define the length we try to expand trivial paths each time
        Extension_Length  = 10
    };

    enum Ambiguous_Depth: unsigned long {//Enum to define the length we try to expand trivial paths each time
        Ambiguous_Depth_Limit  = 100
    };

    enum Ambiguous_Matching_Location: unsigned long {//Enum to define an ambiguous location in matching statistics
        Ambiguous_Loc   = ULONG_MAX,
        Unambiguous_Loc = 1
    };

    suffix_tree stree;
    suffix_tree rtree;
    const parameters  params;
    adjacency_graph agraph;
    std::string next_string;
    std::string next_string_rev;
    std::vector<int> twice_assigned;
    std::vector<bool> assigned;

    ///Alias to make the code look cleaner
    typedef std::pair<unsigned long, unsigned long> Seg_lab_map;
    ///Alias to make the code look cleaner
    typedef std::tuple<unsigned long, unsigned long, Direction> Path_to_extend_lookup;

    std::map<unsigned long, unsigned long> seg_to_label_map; //Mapping between segment probes and labelling probes, map[2]= corresponding label for the second seg probe
    std::list<Path_to_extend_lookup> paths_to_extend; //Mapping between segment probes and labelling probes, map[2]= corresponding label for the second seg probe
    std::vector<unsigned long> nodes_to_remove;
    std::vector<path> path_vector;

    void is_ambiguous(path path_to_test, node_type recent_node, node_type current_node, std::set<path>& results, unsigned long suffix_number, unsigned long depth, Direction dir);
    void expand_subpath(path& path_to_extend, unsigned int node, Direction dir);
    void identify_position();
    void initialize_paths();
    unsigned long expand_path(unsigned long path_index, unsigned long suffix_number, Direction dir);
    unsigned long find_interesting_match(const std::vector<unsigned long>& matchin_stats, const std::vector<unsigned long>& matchin_loc) const;
    void manage_multiple(path& ambiguous_path, unsigned long suffix_number, Direction dir);
    path manage_multiple_simple(path& ambiguous_path, unsigned long suffix_number, Direction dir);
    void write_output() const;
    int find_paths();
    void remove_assigned_nodes();
    bool check_if_resolved(unsigned long recent_node, Direction dir) const;
    std::tuple<int, unsigned long> get_colour_count(const char& col, unsigned long recent_node) const;
    void free_unlocated_path(path& current_path);
    std::string get_colour_string(const path& current_path) const;
    path get_unique_path(const std::set<path>& results) const;
    path get_long_path(std::set<path>& results);
    unsigned long get_recent_node(const path& ambiguous_path, Direction dir) const;
};

#endif //CHROMOTRACE_ORIGINAL_PATHS_H
