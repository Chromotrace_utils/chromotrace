# Chromotrace 

Software to reconstruct the 3D structure of DNA inside the cell using super resolution microscopy data. Publication can be found here https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1006002

CMake must already be installed. This can be found at [CMake](https://cmake.org/)

This code uses the SDSL-lite library which is avaliable here [SDSL-lite](https://github.com/simongog/sdsl-lite)
It will automatically be cloned from the github repository and installed by the cmake script. 

## Install and Usage Instructions

Chromotrace can be built be running the following commands and a binary named chromotrace_new will be built.

```
git clone git@gitlab.com:Chromotrace_utils/chromotrace.git
cd chromotrace
mkdir build
cd build
cmake ..
make
```

Chromotrace can be run by providing a config file in the following way.

```
./chromotrace_new <path to config file>
```

To run chromotrace on the example data edit the config file so that the absolute location of each of the files 
in the config file is specified. The format of the config file format is 

```
<absolute/relative file path to label file>
<absolute/relative file path to segmentation file>
<distance/relative threshold given as decimal number>
<absolute/relative file path to output file>
```

An example config file is given below.

```
/path/to/file/test_data.lab
/path/to/file/test_data.seg
1.0
/path/to/file/test_data.out
```
