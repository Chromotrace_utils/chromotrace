
#include "parameters.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "suffix_tree.h"

/*START: These methods shouldn't be here as they are string methods not parameter methods*/
/// Split string method used as a helper function in the parameters class
/// \param s is the string that will be split
/// \param delim a char representing the delimiter to split the string around
/// \param elems is a vector of strings in which the split string can be stored
void split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

/// A split string method used as a helper in the parameter methods
/// \param s the string that should be split
/// \param delim the deliminting character to split around 
/// \return the split string as a vector of fragments of the original string.
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
/*END*/

/// Get the x co-ordinate at the given position in the vector of x co-ordinates.
/// \param index to retrieve, this is used as a unique identifier for a seg node
/// \return the x value of this node
long double parameters::get_x(const unsigned long index)const{
    return x.at(index);
}

/// Get the y co-ordinate at the given position in the vector of y co-ordinates.
/// \param index to retrieve, this is used as a unique identifier for a seg node
/// \return the y value of this node
long double parameters::get_y(const unsigned long index)const{
    return y.at(index);
}

/// Get the z co-ordinate at the given position in thte vector of z co-ordinates.
/// \param index to retrieve, this is used as a unique identifier for a seg node
/// \return the z value of this node
long double parameters::get_z(const unsigned long index)const{
    return z.at(index);
}

/// method to return the number of probes in the labelling file
/// \return number of probes in the labelling file
unsigned long parameters::get_num_probes() const{
    return num_probes;
}

/// gets the chromosome of the label node at index
/// \param index is a valid index of a label node
/// \return the chromosome of label node index
std::string parameters::get_chromo(const unsigned long index) const {
    return chromosome.at(index);
}

/// gets the start_pos of the label node at index
/// \param index is a valid index of a label node
/// \return the start_pos of label node index
unsigned long parameters::get_start_pos(const unsigned long index) const {
    return start_pos.at(index);
}

/// gets the end_pos of the label node at index
/// \param index is a valid index of a label node
/// \return the end_pos of label node index
unsigned long parameters::get_end_pos(const unsigned long index) const {
    return end_pos.at(index);
}

/// gets the colour of the label node at index
/// \param index is a valid index of a label node
/// \return the colour of label node index
char parameters::get_colour(const unsigned long index) const{
    return colour.at(index);
}

/// Method to process the segment file to put it
/// in a useful format for other methods
/// \param seg_file is the location of the label file on the local
/// filesystem
void parameters::process_seg_file(std::string &seg_file) {//Process the segment file and split the input to arrays
    std::ifstream probe_seg_file(seg_file);

    std::cout << "Seg file: " << seg_file << std::endl;
    if(!probe_seg_file.is_open()){
        std::cerr << "Cannot open seg file: " << seg_file << std::endl;
        exit(EXIT_FAILURE);
    }

    std::string line;
    std::getline(probe_seg_file, line);//Throw away header

    //try to estimate the size needed to avoid resizing, this should over estimate a little
    x = std::vector<long double> ();
    y = std::vector<long double> ();
    z = std::vector<long double> ();

    colour = std::vector<char>();

    auto line_number = 0;

    while(std::getline(probe_seg_file, line)){//process the file
        auto split_string = split(line, '\t');

        long double x_cur;
        long double y_cur;
        long double z_cur;

        std::stringstream(split_string.at(1)) >> x_cur;
        std::stringstream(split_string.at(2)) >> y_cur;
        std::stringstream(split_string.at(3)) >> z_cur;

        x.push_back(x_cur);
        y.push_back(y_cur);
        z.push_back(z_cur);

        char alph_ind;

        std::stringstream(split_string.at(4)) >> alph_ind;

        colour.push_back(alph_ind);
        line_number++;
    }

    num_probes = (unsigned long) line_number-1;
    std::cout << "num_probes: " << num_probes << std::endl;
    std::cout << "x_vec: " << get_x_vec().size()<< std::endl;

}

/// Method to process the labelling file to put it
/// in a useful format for other methods
/// \param label_file is the location of the label file on the local
/// filesystem
void parameters::process_lab_file(std::string &label_file){//Process the labelling file and split the input to arrays

    std::cout << std::endl << "Process Label at " << label_file << std::endl;

    std::ifstream probe_lab_file(label_file);

    if(!probe_lab_file.is_open()){
        std::cerr << "Cannot open label file" << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "Opened label file " << std::endl;
    }

    //try to estimate the size needed
    chromosome = std::vector<std::string> ();
    start_pos  = std::vector<unsigned long> ();
    end_pos    = std::vector<unsigned long> ();

    unsigned long line_number = 0;

    std::string line;
    std::getline(probe_lab_file, line);//Throw away header

    while(std::getline(probe_lab_file, line)){//process the file
        auto split_string = split(line, '\t');

        std::string chr;
        std::stringstream(split_string.at(0)) >> chr;
        chromosome.push_back(chr);

        unsigned long strt;
        unsigned long finsh;

        std::stringstream(split_string.at(1)) >> strt;
        start_pos.push_back(strt);

        std::stringstream(split_string.at(2)) >> finsh;
        end_pos.push_back(finsh);

        label_string.append(split_string.at(3));//String for suffix tree

        line_number++;
    }

    std::cout << "Read label" << std::endl;
}

/// Method to process the labelling file to put it
/// in a useful format for other methods
/// \param label_file is the location of the label file on the local
/// filesystem
void parameters::process_2_col_lab_file(std::string &label_file){//Process the labelling file and split the input to arrays

    std::cout << std::endl << "Process Label at " << label_file << std::endl;

    std::ifstream probe_lab_file(label_file);

    if(!probe_lab_file.is_open()){
        std::cerr << "Cannot open label file" << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "Opened label file " << std::endl;
    }

    //try to estimate the size needed
    chromosome = std::vector<std::string> ();
    start_pos  = std::vector<unsigned long> ();
    end_pos    = std::vector<unsigned long> ();

    unsigned long line_number = 0;

    std::string line;
    std::getline(probe_lab_file, line);//Throw away header

    while(std::getline(probe_lab_file, line)){//process the file

        std::istringstream buffer(line);
        std::vector<std::string> split_string{std::istream_iterator<std::string>(buffer),
                                     std::istream_iterator<std::string>()};
        std::cout << line << std::endl;
        std::cout << "split string size: " << split_string.size() << std::endl;

        chromosome.emplace_back("1");

        unsigned long strt;
        unsigned long alpha_inx;

        std::stringstream(split_string.at(0)) >> strt;
        std::stringstream(split_string.at(1)) >> alpha_inx;

        start_pos.push_back(strt);

        end_pos.push_back(strt+9999);

        label_string.push_back(alpha.at(alpha_inx));//String for suffix tree

        line_number++;
    }

    std::cout << "Read label" << std::endl;
}


/// Method to process all the parameters that will be used
/// by the rest of the algorithm
/// \param filename is the location of the config file on the local
/// filesystem
void parameters::read_params(std::string &filename){
    std::ifstream conf_file(filename);
    std::string line;

    if(!conf_file.is_open()){
        std::cerr << "Cannot open config file: " << filename << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "Opened config file " << std::endl;
    }

    int line_number = 0;
    while (std::getline(conf_file,line)) {
        switch(line_number){
            case 0:
                std::cout << "Process Label file: " << line.c_str()  << std::endl;
                process_lab_file(line);
                break;
            case 1:
                std::cout << "Process segment file" << std::endl;
                process_seg_file(line);
                break;
            case 2:
                threshold = std::stof(line);
                std::cout << "Threshold: " << threshold << std::endl;
                break;
            case 3:
                output_file = line;
                break;
            default:
                break;
        }
        line_number++;
    }

    conf_file.close();
    std::cout << "Processed all files"  << std::endl;
}

/// Method to process all the parameters that will be used
/// by the rest of the algorithm
/// \param filename is the location of the config file on the local
/// filesystem
void parameters::read_params(std::string &filename, std::string &output_location, float new_threshold){
    std::ifstream conf_file(filename);
    std::string line;

    if(!conf_file.is_open()){
        std::cerr << "Cannot open config file: " << filename << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "Opened config file " << std::endl;
    }

    int line_number = 0;
    while (std::getline(conf_file,line)) {
        switch(line_number){
            case 0:
                std::cout << "Process Label file: " << line.c_str()  << std::endl;
                process_lab_file(line);
                break;
            case 1:
                std::cout << "Process segment file" << std::endl;
                process_seg_file(line);
                break;
            case 2:
                threshold = new_threshold;
                std::cout << "Threshold: " << threshold << std::endl;
                break;
            case 3:
                output_file = output_location;
                break;
            default:
                break;
        }
        line_number++;
    }

    conf_file.close();
    std::cout << "Processed all files"  << std::endl;
}

/// Get the label string which is all colours concatonated
/// together
/// \return std::string& reference to the location of the string
const std::string& parameters::getLabel_string() const {
    return label_string;
}

/// Get the label string which is all colours concatonated
/// together reversed.
/// \return std::string of the reversered label_string
const std::string parameters::getLabel_string_reverse() const {
    std::string reverse = std::string(label_string);
    std::reverse(std::begin(reverse),std::end(reverse));;
    return reverse;//this should be moved by compiler
}

/// Get the label string which is all colours concatonated
/// together
/// \return std::string& reference to the location of the string
const std::string parameters::getLabel_sub_string(const unsigned long start, const unsigned long length) const {
    return label_string.substr(((start>label_string.size())? 0 : start),(start<length) ? : length);
}

/// Return the threshold used to determin adjacency
/// \return the threshold used to determin adjacency
float parameters::get_threshold() const {
    return threshold;
}

/// Returns pointer to char array specifying the output file
/// \return pointer to char array specifying the output file
std::string parameters::get_output_file() const {
    return output_file;
}

/// Returns vector of all x co-ordinates
/// \return vector of all x co-ordinates
std::vector<long double> parameters::get_x_vec() const {
    return x;
}
/// Returns vector of all y co-ordinates
/// \return vector of all y co-ordinates
std::vector<long double> parameters::get_y_vec() const {
    return y;
}
/// Returns vector of all z co-ordinates
/// \return vector of all z co-ordinates
std::vector<long double> parameters::get_z_vec() const {
    return z;
}
