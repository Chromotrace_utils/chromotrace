
#include <iostream>

#include "parameters.h"
#include "suffix_tree.h"
#include "adjacency_graph.h"
#include "chromotrace.h"

/// A simmple method to run the main chromotrace algorithm
///
/// \param configfile A config file to pass the needed parameters to the parameters object 
void run(std::string &configfile){

    std::cout << "Loading Parameters" << std::endl;
    std::cout << "Config File Name: " << configfile << std::endl;

    parameters params(configfile);

    std::cout << "Building Suffix trees"  << std::endl;
    std::cout << params.getLabel_string() << std::endl;

    suffix_tree stree = suffix_tree(params.getLabel_string());//Uses new suffix tree class. Doesn't store suffix trees
    std::cout << "Building reverse"     << std::endl;
    suffix_tree rtree = suffix_tree(params.getLabel_string_reverse());

    std::cout << "Built Suffix trees" << std::endl;

    adjacency_graph adjacency_graph(params.get_x_vec(),params.get_y_vec(),params.get_z_vec(), params.get_threshold());

    std::cout << "Building paths"     << std::endl;
    chromotrace ctrace(params, adjacency_graph, stree, rtree);//Passed by ref, maybe should just give ownership to ctrace?
}

/// A simmple method to run the main chromotrace algorithm
///
/// \param configfile A config file to pass the needed parameters to the parameters object
void run(std::string &configfile, std::string &output_file, float threshold){

    std::cout << "Loading Parameters" << std::endl;
    std::cout << "Config File Name: " << configfile << std::endl;

    parameters params(configfile, output_file, threshold);

    std::cout << "Building Suffix trees"  << std::endl;
    std::cout << params.getLabel_string() << std::endl;

    suffix_tree stree = suffix_tree(params.getLabel_string());//Uses new suffix tree class. Doesn't store suffix trees
    std::cout << "Building reverse"     << std::endl;
    suffix_tree rtree = suffix_tree(params.getLabel_string_reverse());

    std::cout << "Built Suffix trees" << std::endl;

    adjacency_graph adjacency_graph(params.get_x_vec(),params.get_y_vec(),params.get_z_vec(), params.get_threshold());

    std::cout << "Building paths"     << std::endl;
    chromotrace ctrace(params, adjacency_graph, stree, rtree);//Passed by ref, maybe should just give ownership to ctrace?
}