//bsub -M100000 -Is ./recon_algorithm ../input_params.real.txt

/*
ARG1 > The file containing the input parameters it has to have the following format
path_to_lab_file
path_to_seg_file
physical_distance_threshold
output_file
*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "run_algorithm.h"

/// A method to start the execution of the main algorithm.
/// \param argc The number of arguments passed to the main method 
/// \param argv The arguments passed to the main method 
/// \return an error code related to the execution of the main algorithm

int main(int argc, char *argv[]){

    std::string paramfile;
    std::string output_file;

    if(argc < 2){
        std::cerr << "Not enough arguments specified. Program can be run as shown below." << std::endl;
        std::cerr << "chromotrace_new /path/to/config/file" << std::endl;
        exit(EXIT_FAILURE);
    } else if (argc == 4) {
        paramfile = std::string(argv[1]);
        output_file = std::string(argv[2]);
        run(paramfile, output_file, std::stof(argv[3]));
    } else {
        paramfile = std::string(argv[1]);
        run(paramfile);
    }


    exit(EXIT_SUCCESS);
}



