
/// Created by carl on 30/09/16.
///A suffix tree class based on the SDSL
///library providing the basic functionality for
///chromotrace to use the suffix tree.

#ifndef CHROMOTRACE_NEW_SUFFIXTREE_H
#define CHROMOTRACE_NEW_SUFFIXTREE_H
#include "parameters.h"
#include <stdio.h>
#include <sdsl/suffix_trees.hpp>
#include <cstring>
#include <zconf.h>
#include "limits.h"


using namespace sdsl;

//Typedefs for convenience
typedef sdsl::cst_sada<>::node_type node_type;
typedef sdsl::cst_sada<> cst_t;
typedef sdsl::int_vector<>::size_type size_type;

/// Suffix tree class
class suffix_tree {

private:


    cst_t cst;
    unsigned long text_length;
    unsigned long min_depth;
    std::vector<unsigned long> marked; //Mapping between segment probes and labelling probes, map[2]= corresponding label for the second seg probe
    std::vector<bool> unique;
    std::vector<unsigned long> unique_node; //Mapping between segment probes and labelling probes, map[2]= corresponding label for the second seg probe
    std::vector<char> alpha = {'#','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

public:

    suffix_tree() : text_length(0), cst() {}

    /// Primary constructor for building a suffix tree based on the text.
    /// \param text the text to build the suffix tree of
    explicit suffix_tree(const std::string& text): text_length(text.size()){

        if(text_length==ULONG_MAX){
            std::cout << "Text is too long" << std::endl;
        }else{
            std::string file = "@testinput.iv8";
            store_to_file(text.c_str(), file);
            construct(cst, file,1);

            if(cst.size()>0)
                std::cout << "Suffix tree loaded successfully" << std::endl;
            else
                std::cout << "Error loading suffix tree" << std::endl;

            min_depth = compute_unique_min_depth();
        }
    }

    //Below is defined in two classes, should be imported by one?
    enum Ambiguous_Matching_Location: unsigned long {//Enum to define an ambiguous location in matching statistics
        Ambiguous_Loc = ULONG_MAX
    };

    unsigned long compute_unique_min_depth() const;//Doesn't take into account fixed depth
    unsigned long get_min_depth() const;
    unsigned long get_text_length() const;

    std::tuple<std::vector<unsigned long>,std::vector<unsigned long>> unique_matching_statistics(std::string pattern) const;//compute matching statistics for pattern

    node_type is_unique(const std::string& pattern, unsigned long depth) const;//Simulate truncated suffix trees by only doing this for fixed depth
    node_type get_suffix_leaf(unsigned long suffix) const;
    node_type get_parent(node_type node) const;
    node_type get_root() const;

    std::string get_suffix(unsigned long suffix, unsigned long length);
    std::string get_suffix(unsigned long suffix);

    size_type get_size() const;
    bool is_root(node_type node) const;
    bool valid_pos(unsigned long position) const;
    void mark_node(node_type node);
    void mark_leaf(unsigned long node);


};


#endif //CHROMOTRACE_ORIGINAL_SUFFIX_TREE_H
