//
// Created by Odyss on 09/03/2020.
//


#include <gtest/gtest.h>
#include "../src/suffix_tree.h"
#include <gmock/gmock.h>

class SuffFunctionTest: public ::testing::Test {
public:
    SuffFunctionTest( ) {
        // initialization;
        // can also be done in SetUp()
    }

    suffix_tree test;
    suffix_tree test2;

    void SetUp( ) {
        test  = suffix_tree("gggggggaaccgggggaacc");
        test2 = suffix_tree("acgtgtcgtagcgtgatgcg");

        std::cout << "TEST RUNNING: " << test.get_size() << std::endl;
        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~SuffFunctionTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};


TEST_F (SuffFunctionTest /*test suite name*/, LoadTreeTest /*test name*/) {
    ASSERT_EQ (test.get_size(), 20);     // success
    ASSERT_EQ (test2.get_size(), 20);     // success

}

TEST_F (SuffFunctionTest /*test suite name*/, MatchingStatsTest1 /*test name*/) {

    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test.unique_matching_statistics("aacc");
    ASSERT_EQ (matching_stats.size(), 4);
    ASSERT_EQ (marked_nodes.size(), 4);

    std::vector<unsigned long> expected       = {4, 3, 2, 1};
    std::vector<unsigned long> expected_nodes = {suffix_tree::Ambiguous_Loc, suffix_tree::Ambiguous_Loc, suffix_tree::Ambiguous_Loc, suffix_tree::Ambiguous_Loc};

    for (auto count = 0; count < matching_stats.size(); ++count) {
        ASSERT_EQ(matching_stats.at(count), expected.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes.at(count));

    }
    std::cout << std::endl;

    std::tie(matching_stats, marked_nodes) = test.unique_matching_statistics("aaccgg");
    ASSERT_EQ (matching_stats.size(), 6);
    ASSERT_EQ (marked_nodes.size(), 6);

    std::vector<unsigned long> expected1 = {6, 5, 4, 3, 2, 1};
    std::vector<unsigned long> expected_nodes1 = {7, 8, 9, 10, suffix_tree::Ambiguous_Loc, suffix_tree::Ambiguous_Loc};

    for (auto count = 0; count < matching_stats.size(); ++count) {
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes1.at(count));

    }
    std::cout << std::endl;

    std::tie(matching_stats, marked_nodes) = test.unique_matching_statistics("dddddddddd");
    ASSERT_EQ (matching_stats.size(), 10);
    ASSERT_EQ (marked_nodes.size(), 10);

    std::vector<unsigned long> expected2 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, };
    std::vector<unsigned long> expected_nodes2 = {suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                                  suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                                  suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc};

    for (auto count = 0; count < matching_stats.size(); ++count) {
        ASSERT_EQ(matching_stats.at(count), expected2.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes2.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test.unique_matching_statistics("ggggtttttggggg");
    ASSERT_EQ (matching_stats.size(), 14);
    ASSERT_EQ (marked_nodes.size(), 14);

    std::vector<unsigned long> expected3 = {4,3,2,1,0,0,0,0,0,5,4,3,2,1};
    std::vector<unsigned long> expected_nodes3 = {suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                                  suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                                  suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected3.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes3.at(count));
    }
}

TEST_F (SuffFunctionTest, MatchingStatsTest2 ) {

    std::vector<unsigned long> matching_stats;
    std::vector<unsigned long> marked_nodes;

    std::tie(matching_stats, marked_nodes) = test2.unique_matching_statistics("gtgttagggt");
    ASSERT_EQ (matching_stats.size(), 10);
    ASSERT_EQ (marked_nodes.size(), 10);

    std::vector<unsigned long> expected = {4,3,2,1,3,2,1,1,2,1};
    std::vector<unsigned long> expected_nodes = {2,3,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,8,9,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.unique_matching_statistics("cgtgtcgtgat");
    ASSERT_EQ (matching_stats.size(), 11);
    ASSERT_EQ (marked_nodes.size(), 11);

    std::vector<unsigned long> expected1 = {8,7,6,5,4,6,5,4,3,2,1};
    std::vector<unsigned long> expected_nodes1 = {1,2,3,4,5,11,12,13,14,15,suffix_tree::Ambiguous_Loc};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected1.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes1.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.unique_matching_statistics("cccccacgtgtcgtagcgtgatgcggggggg");
    ASSERT_EQ (matching_stats.size(), 31);
    ASSERT_EQ (marked_nodes.size(), 31);


    std::vector<unsigned long> expected2 = {1,1,1,1,1,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,1,1,1,1,1,1};
    std::vector<unsigned long> expected_nodes2 = {suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                            suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
                                            suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                            suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                            suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected2.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes2.at(count));
    }

    std::tie(matching_stats, marked_nodes) = test2.unique_matching_statistics("tatcatgctagcgtgatgcg");

    std::vector<unsigned long> expected3 = {2,2,2,1,4,3,2,1,12,11,10,9,8,7,6,5,4,3,2,1};
    std::vector<unsigned long> expected_nodes3 = {8,15,5,suffix_tree::Ambiguous_Loc,15,16,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,
                                                  8,9,10,11,12,13,14,15,16,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc,suffix_tree::Ambiguous_Loc};

    for(auto count=0; count< matching_stats.size(); ++count){
        ASSERT_EQ(matching_stats.at(count), expected3.at(count));
        ASSERT_EQ(marked_nodes.at(count), expected_nodes3.at(count));
    }
}

