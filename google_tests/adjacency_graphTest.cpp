//
// Created by Odyss on 03/03/2020.
//

#include <gtest/gtest.h>
#include "../src/adjacency_graph.h"
#include <gmock/gmock.h>

class AdjGraphFunctionTest: public ::testing::Test {
public:
    AdjGraphFunctionTest( ) {
        // initialization;
        // can also be done in SetUp()
    }

    adjacency_graph test;
    adjacency_graph test2;
    adjacency_graph test3;

    void SetUp( ) {

        std::vector<long double> x_vec = {0,1,1,1,2,2,2,3,3,3};
        std::vector<long double> y_vec = {0,0,1,1,1,2,2,2,3,3};
        std::vector<long double> z_vec = {0,0,0,1,1,1,2,2,2,3};

        float threshold = 1.0;

        test  = adjacency_graph(x_vec, y_vec, z_vec, threshold);

        threshold = 2.0;

        test2 = adjacency_graph(x_vec, y_vec, z_vec, threshold);

        x_vec = {4,3,4,4,5,4,4,3,4,8};
        y_vec = {4,4,3,4,4,5,4,3,3,8};
        z_vec = {4,4,4,3,4,4,5,3,3,8};

        threshold = 1.0;

        test3  = adjacency_graph(x_vec, y_vec, z_vec, threshold);

        std::cout << "TEST RUNNING: " << std::endl;
        // initialization or some code to run before each test
    }

    void TearDown( ) {
        // code to run after each test;
        // can be used instead of a destructor,
        // but exceptions can be handled in this function only
    }

    ~AdjGraphFunctionTest( )  {
        //resources cleanup, no exceptions allowed
    }

    // shared user data
};


TEST_F(AdjGraphFunctionTest, SizeTest) {
    ASSERT_EQ(test.get_num_seg_probes(),10);
    ASSERT_EQ(test2.get_num_seg_probes(),10);
    ASSERT_EQ(test3.get_num_seg_probes(),10);
}

TEST_F(AdjGraphFunctionTest, DegreeTest) {
    ASSERT_EQ(test.get_degree(0),1);
    ASSERT_EQ(test2.get_degree(0),3);
    ASSERT_EQ(test3.get_degree(9),0);
    ASSERT_EQ(test.get_degree(3),2);
    ASSERT_EQ(test2.get_degree(4),6);
    ASSERT_EQ(test3.get_degree(7),1);
}

TEST_F(AdjGraphFunctionTest, MinDegreeTest) {
    ASSERT_EQ(test.getMinDegree() ,1);
    ASSERT_EQ(test2.getMinDegree(),3);
    ASSERT_EQ(test3.getMinDegree(),0);
}

TEST_F(AdjGraphFunctionTest, MaxDegreeTest) {
    ASSERT_EQ(test.getMaxDegree(), 2);
    ASSERT_EQ(test2.getMaxDegree(),6);
    ASSERT_EQ(test3.getMaxDegree(),6);
}

TEST_F(AdjGraphFunctionTest, RemoveNodeTest) {

    test.remove_node(0);
    ASSERT_EQ(test.get_num_seg_probes(),9);

    test.remove_node(4);
    test.remove_node(5);
    test.remove_node(6);
    ASSERT_EQ(test.get_num_seg_probes(),6);

    ASSERT_EQ(test.getInitialNumSegProbes(),10);
    test2.remove_node(0);
    ASSERT_EQ(test2.get_num_seg_probes(),9);
    ASSERT_EQ(test2.getInitialNumSegProbes(),10);
    test3.remove_node(0);
    ASSERT_EQ(test3.get_num_seg_probes(),9);
    ASSERT_EQ(test3.getInitialNumSegProbes(),10);
}

TEST_F(AdjGraphFunctionTest, GetAdjNodeTest) {

    std::list<unsigned long> adj_list = {2,4};

    for(auto iter = test.get_adj_nodes_front(3); iter!=test.get_adj_nodes_back(3); ++iter){
        ASSERT_EQ(*(iter), adj_list.front());
        adj_list.pop_front();
    }

    adj_list = {6,8};

    for(auto iter = test.get_adj_nodes_front(7); iter!=test.get_adj_nodes_back(7); ++iter){
        ASSERT_EQ(*(iter), adj_list.front());
        adj_list.pop_front();
    }

    adj_list = {1,2,3,5,6,7};

    for(auto iter = test2.get_adj_nodes_front(4); iter!=test2.get_adj_nodes_back(4); ++iter){
        ASSERT_EQ(*(iter), adj_list.front());
        adj_list.pop_front();
    }

    adj_list = {0,2,3,4};

    for(auto iter = test2.get_adj_nodes_front(1); iter!=test2.get_adj_nodes_back(1); ++iter){
        ASSERT_EQ(*(iter), adj_list.front());
        adj_list.pop_front();
    }

    adj_list = {};
    ASSERT_EQ(adj_list.size(), adj_list.size());

    adj_list = {1,2,3,4,5,6};

    for(auto iter = test3.get_adj_nodes_front(0); iter!=test3.get_adj_nodes_back(0); ++iter){
        ASSERT_EQ(*(iter), adj_list.front());
        adj_list.pop_front();
    }

}