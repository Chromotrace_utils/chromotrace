
#include "chromotrace.h"

/// Safely remove assigned nodes from the distance matrix.
/// This method will remove an assigned node if it is not the head
/// or tail of an active path
///
void chromotrace::remove_assigned_nodes() {

    std::cout << "Remove assigned nodes from agraph" << std::endl;
    for(auto const & path_current : path_vector){
        auto front = path_current.get_front_iterator();
        auto last  = path_current.get_last_node();

        ++front;

        while(*front!=last && assigned.at(*front)){
            std::cout << "Remove " << *front << std::endl;
            agraph.remove_node(*front);

            ++front;
        }
    }
}

/// Identify non-abiguous paths in the distance matrix.
/// This method will identify potential starting points of
/// these paths using extend_subpath to actually compute it.
///
void chromotrace::initialize_paths(){

    for(auto node_index = 0; node_index<agraph.getInitialNumSegProbes(); node_index++){

        path current_path;

        std::vector<char> char_vec = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','w','x','y','z'};

        for(auto check_char =0; check_char<20; ++check_char ){


            auto count = std::count(next_string.begin(), next_string.end(), char_vec.at(check_char));

            if ((!assigned.at(node_index)) && (count == 1) && (params.get_colour(node_index) == char_vec.at(check_char))){
                path current_best_ext = path();
                current_best_ext.add_first_node(node_index);
                std::cout << "Unique node " << char_vec.at(check_char) << std::endl;

                assigned.at(node_index) = true;

                path_vector.push_back(current_best_ext);
            }
        }

        if(!assigned.at(node_index) && (agraph.get_degree(node_index)==2)){
            current_path.add_first_node(node_index);

            auto first_neigh  = *agraph.get_adj_nodes_front(node_index);
            auto second_neigh = *(++agraph.get_adj_nodes_front(node_index));

            assigned.at(node_index) = true;

            assert(node_index<= agraph.getInitialNumSegProbes());//Assert valid node
            assert(node_index>0);//Assert valid node

            expand_subpath(current_path, first_neigh,  Forward);
            expand_subpath(current_path, second_neigh, Reverse);

            path_vector.push_back(current_path);
        }
    }

    /*
    for(auto node_index = 0; node_index<agraph.get_num_seg_probes(); node_index++) {
        if(!assigned[node_index]) {
            path current_best_ext=path();

            std::cout << "Push single node" << std::endl;

            //assigned[node_index] = true;

            for (int suffix_number = 0; suffix_number < next_string.size(); suffix_number++) {

                if (next_string[suffix_number] == params.get_colour(node_index)){
                    path current_path;
                    current_path.add_first_node(node_index);

                    //std::cout << "Initial Size: " << current_path.get_size() << std::endl;
                    auto fwd_ext  = manage_multiple_simple(current_path, suffix_number, Forward);
                    //fwd_ext = (fwd_ext.get_size()) ? fwd_ext : path();
                    //std::cout << "Forward Size: " << fwd.get_size() << std::endl;
                    auto bkwd_ext = manage_multiple_simple(current_path, suffix_number, Reverse);
                    //bkwd_ext = (bkwd_ext.get_size()) ? bkwd_ext : path();
                    //std::cout << "Backward Size: " << bkwd.get_size() << std::endl;
                    current_best_ext = (current_best_ext.get_size()>fwd_ext.get_size()) ? current_best_ext : fwd_ext ;
                    current_best_ext = (current_best_ext.get_size()>bkwd_ext.get_size()) ? current_best_ext : bkwd_ext;
                }
            }
            if(current_best_ext.get_size()>10) {
                std::cout << "Add posible path of length: " << current_best_ext.get_size() << std::endl;
                auto front = current_best_ext.get_front_iterator();
                auto back  = current_best_ext.get_end_iterator();

                while(front !=back){
                    assigned[*front] = true;
                    std::cout << params.get_colour(*front) ;
                    front++;
                }
                std::cout << std::endl;
                path_vector.push_back(current_best_ext);
            }
        }
    }*/

    path_vector.shrink_to_fit();
    std::cout << "Found " << path_vector.size() << " trivial paths" << std::endl;

}

/// Recursive method to extend the path in one direction until it
/// reaches an ambiguous node.
///
/// \param path_to_extend path variable which we want to try and extend
/// \param node a node which is a possibly extension for the current path.
/// \param dir direction to extend
void chromotrace::expand_subpath(path &path_to_extend, const unsigned int node, const Direction dir){
    if(!assigned.at(node)){//If not already in a path
        unsigned long previous_node;

        assert(node <= agraph.getInitialNumSegProbes());//Assert valid node
        assert(node>0);//Assert valid node

        if(dir) {
            previous_node = path_to_extend.get_first_node();
            path_to_extend.add_first_node(node);
        } else {
            previous_node = path_to_extend.get_last_node();
            path_to_extend.add_last_node(node);
        }

        assigned.at(node) = true;

        if(agraph.get_degree(node)==2){//Continue or kill recursion
            auto new_node = (*agraph.get_adj_nodes_front(node) != previous_node) ? *agraph.get_adj_nodes_front(node) : *(++agraph.get_adj_nodes_front(node));//Get node that doesn't equal last node
            expand_subpath(path_to_extend, new_node, dir);
        }
    }
}

/// Method to create a mapping between the trivial paths and their
/// genomic location.
///
void chromotrace::identify_position(){

    auto loc_path_count = 0, total_probe_count = 0;
    unsigned long max_path_length = 0;

    std::cout << "Unique min "<< stree.compute_unique_min_depth() << std::endl;

    for(auto vector_count = 0; vector_count<path_vector.size(); vector_count++){
        path current_path = path_vector.at(vector_count);
        //std::cout << "Colour string for " << vector_count << ": " << get_colour_string(current_path) << std::endl;

        if(current_path.get_size()!=0 && seg_to_label_map.count(current_path.get_first_node())==0) {
            std::string colour_string = get_colour_string(current_path);
            //std::cout << colour_string << std::endl;
            std::vector<unsigned long> smatching_loc, rmatching_loc, smatching_stats, rmatching_stats;

            std::tie(smatching_stats, smatching_loc) = stree.unique_matching_statistics(colour_string);
            //std::cout << "finished forward matching stats" << std::endl;

            std::tie(rmatching_stats, rmatching_loc) = rtree.unique_matching_statistics(colour_string);
            //std::cout << "finished all matching stats" << std::endl;

            auto smax_loc = find_interesting_match(smatching_stats, smatching_loc);//Location of max element
            auto rmax_loc = find_interesting_match(rmatching_stats, rmatching_loc);//Location of max element

            if ((smatching_stats.at(smax_loc) > rmatching_stats.at(rmax_loc)) &&
                 smatching_stats.at(smax_loc) > rmatching_stats.at(smax_loc)) {

                auto to_remove = smatching_stats.size() - 1 - smax_loc - smatching_stats.at(smax_loc);
                for (auto temp_count = 0; temp_count < smax_loc; temp_count++) {
                    assigned.at(current_path.get_first_node()) = false;
                    current_path.remove_first_node();
                }
                for (auto temp_count = 0; temp_count < to_remove; temp_count++) {
                    assigned.at(current_path.get_last_node()) = false;
                    current_path.remove_last_node();
                }

                path_vector.at(vector_count) = current_path;

                auto path_iter = current_path.get_front_iterator();
                auto start_pos = smatching_loc.at(smax_loc);
                auto count = 0;

                loc_path_count++;
                total_probe_count += smatching_stats.at(smax_loc);
                max_path_length = (smatching_stats.at(smax_loc) > max_path_length) ? smatching_stats.at(smax_loc) : max_path_length;

                while (count < smatching_stats.at(smax_loc)) {
                    assert(*(path_iter) < agraph.getInitialNumSegProbes());
                    assert(stree.valid_pos(start_pos));
                    assert(smatching_stats.at(smax_loc) == current_path.get_size());
                    //stree.mark_leaf(start_pos+1);
                    //rtree.mark_leaf(rtree.get_text_length()-start_pos+1);
                    ++twice_assigned.at(start_pos);
                    if(twice_assigned.at(start_pos)==2){
                        next_string.at(start_pos)     = 'A';
                        next_string_rev.at(rtree.get_text_length()-start_pos-1) = 'A';
                    }

                    seg_to_label_map.insert(Seg_lab_map(*(path_iter++), start_pos++));
                    count++;
                }

                paths_to_extend.emplace_back(Path_to_extend_lookup(vector_count, start_pos, Forward));
                paths_to_extend.emplace_back(
                        Path_to_extend_lookup(vector_count, start_pos - smatching_stats.at(smax_loc), Reverse));

            } else if (rmax_loc != (rmatching_stats.size() - 1) &&
                       rmatching_stats.at(rmax_loc) > smatching_stats.at(rmax_loc)) {

                auto to_remove = rmatching_stats.size() - 1 - rmax_loc - rmatching_stats.at(rmax_loc);
                for (auto temp_count = 0; temp_count < rmax_loc; temp_count++) {
                    assigned.at(current_path.get_first_node()) = false;
                    current_path.remove_first_node();
                }
                for (auto temp_count = 0; temp_count < to_remove; temp_count++) {
                    assigned.at(current_path.get_last_node()) = false;
                    current_path.remove_last_node();
                }

                path_vector.at(vector_count) = current_path;

                auto path_iter = current_path.get_front_iterator();
                auto start_pos = rtree.get_text_length() - rmatching_loc.at(rmax_loc) - 1;
                auto count = 0;

                loc_path_count++;
                total_probe_count += rmatching_stats.at(rmax_loc);
                max_path_length = (rmatching_stats.at(rmax_loc) > max_path_length) ? rmatching_stats.at(rmax_loc)
                                                                                : max_path_length;

                while (count < rmatching_stats.at(rmax_loc)) {
                    assert(*(path_iter) < agraph.getInitialNumSegProbes());
                    assert(stree.valid_pos(start_pos));
                    assert(rmatching_stats.at(rmax_loc) == current_path.get_size());
                    ++twice_assigned.at(start_pos);
                    if(twice_assigned.at(start_pos)==2){
                        next_string.at(start_pos)     = 'A';
                        next_string_rev.at(rtree.get_text_length()-start_pos-1) = 'A';
                    }

                    seg_to_label_map.insert(Seg_lab_map(*(path_iter++), start_pos--));
                    count++;
                }

                paths_to_extend.emplace_back(
                        Path_to_extend_lookup(vector_count, start_pos + rmatching_stats.at(rmax_loc) + 1, Forward));
                paths_to_extend.emplace_back(Path_to_extend_lookup(vector_count, start_pos + 1, Reverse));
            } else if(colour_string.length()==2 && smatching_loc.at(0)!=Ambiguous_Loc) {
                path_vector.at(vector_count) = current_path;

                auto path_iter = current_path.get_front_iterator();
                auto start_pos = smatching_loc.at(smax_loc);

                std::cout << "Map single node: " << colour_string << std::endl;
                seg_to_label_map.insert(Seg_lab_map(*(path_iter), start_pos));

                paths_to_extend.emplace_back(Path_to_extend_lookup(vector_count, start_pos + rmatching_stats.at(rmax_loc) + 1, Forward));
                paths_to_extend.emplace_back(Path_to_extend_lookup(vector_count, start_pos + 1, Reverse));

            } else {
                free_unlocated_path(current_path);
                path_vector.at(vector_count) = path();
            }
        }
    }

    auto total_probe_count_2 = 0;
    for(int counter =0 ;counter <agraph.getInitialNumSegProbes();counter++){

        total_probe_count_2 += assigned.at(counter);
    }

    std::cout << " Start looping "<< seg_to_label_map.size() << std::endl;


    std::cout << "Total Number of Probes Located: "   << total_probe_count_2 << std::endl;
    std::cout << "Out of: "        << agraph.getInitialNumSegProbes()          << std::endl;
    std::cout << "Max Path Size: " << max_path_length << std::endl;
    std::cout << "Total Number of Paths Located: "    << loc_path_count    << std::endl;
}

/// Take an array of matching statistics and matching locations
/// and determine the longest non-ambiguous match.
/// \param matching_stats is the address of a vector that contains the matching statistics
/// \param matching_loc is the address of a vector that contains a matching location for the matching statistics 
/// \return return the index of the interesting location.
unsigned long chromotrace::find_interesting_match(const std::vector<unsigned long>& matching_stats, const std::vector<unsigned long>& matching_loc) const{
    unsigned long max_loc = matching_loc.size()-1;
    for(unsigned long counter = 0; counter<matching_stats.size(); counter++){
        if(matching_loc.at(counter)!=Ambiguous_Loc && matching_stats.at(counter)>matching_stats.at(max_loc))
            max_loc = counter;
    }//Either reach the end or the first interesting max

    return max_loc;
}

/// Take trivial paths and extend them until they are ambiguous
/// \param current_path is the current path to try to extend
/// \param suffix_number the identifier of the suffix that has already been matched
/// \param dir is the direction to extend in
/// \return return the last suffix that has been mapped
unsigned long chromotrace::expand_path(const unsigned long path_index, const unsigned long suffix_number, const Direction dir){//TODO: Clean up this method, should anything be pulled out into its own method?
    path current_path = path_vector.at(path_index);
    std::string suffix_remaining;
    unsigned long last_suffix = 0;
    bool used=false;

    std::cout << "Trying to expand " << get_colour_string(current_path) << std::endl;
    node_type recent_node = get_recent_node(current_path, dir);


    last_suffix      = seg_to_label_map.at(recent_node);


    if(dir){
        suffix_remaining = params.getLabel_sub_string((suffix_number< Extension_Length) ? 0 : suffix_number-Extension_Length, (suffix_number< Extension_Length) ? suffix_number : Extension_Length);//Do not want the entire suffix as it might be very long;
        std::reverse(suffix_remaining.begin(), suffix_remaining.end());
    } else {
        suffix_remaining = params.getLabel_sub_string(suffix_number, Extension_Length);//Do not want the entire suffix as it might be very long;
    }

    bool ambiguous   = false, first_left  = seg_to_label_map.at(current_path.get_last_node()) > seg_to_label_map.at(current_path.get_first_node());

    for(char& col : suffix_remaining){//Stop this loop when we find an ambiguous position
        auto col_count = 0, position = 0;
        std::tie(col_count, position) = get_colour_count(col, recent_node);

        if(col_count==Unambiguous_Loc && !assigned.at(position) && !check_if_resolved(recent_node, dir)) {
            recent_node = position;
            assert(recent_node <= agraph.getInitialNumSegProbes());
            if (dir) {//Reverse
                (first_left) ? current_path.add_first_node(position) : current_path.add_last_node(position);
                --last_suffix;
            } else {//Forward
                (!first_left) ? current_path.add_first_node(position) : current_path.add_last_node(position);
                ++last_suffix;
            }
            assert(stree.valid_pos(last_suffix) || last_suffix < agraph.getInitialNumSegProbes());
            ++twice_assigned.at(last_suffix);

            if(twice_assigned.at(last_suffix)==2){
                next_string.at(last_suffix)     = 'A';
                next_string_rev.at(rtree.get_text_length()-last_suffix-1) = 'A';
            }

            seg_to_label_map.insert(Seg_lab_map(position, last_suffix));

            assigned.at(position) = true;
            used      = true;

        } else {
            ambiguous = (col_count != Unambiguous_Loc);
            used      = assigned.at(position);
            break;
        }
    }



    std::cout << "End unambiguous Extension "<< std::endl;

    path_vector.at(path_index) = current_path;//Save the changes we just made just incase we end up at expand_path again
    if(!ambiguous && !used){//Is it unambiguous extension?
        if(dir && stree.valid_pos(suffix_number-Extension_Length))//Reverse
            last_suffix = expand_path(path_index, suffix_number-Extension_Length, dir);
        else if(stree.valid_pos(suffix_number+Extension_Length))//Forward
            last_suffix = expand_path(path_index, suffix_number+Extension_Length, dir);
    } else if (!used){//Or ambiguous extension?
        std::cout << "Extension might be ambiguous " << std::endl;
        if(dir && stree.valid_pos(last_suffix)) {//Reverse
            manage_multiple(current_path, last_suffix - 1, dir);
        } else if(stree.valid_pos(last_suffix)) {//Forward
            manage_multiple(current_path, last_suffix + 1, dir);
        }
        auto temp_left  = seg_to_label_map.at(current_path.get_last_node()) > seg_to_label_map.at(current_path.get_first_node());

        if(temp_left) {
            last_suffix = seg_to_label_map.at(current_path.get_first_node());
        }
        else {
            last_suffix = seg_to_label_map.at(current_path.get_last_node());
        }
        path_vector.at(path_index) = current_path;
    }//This is probably quite a lazy method

    return last_suffix;
}

path chromotrace::manage_multiple_simple(path& ambiguous_path, const unsigned long suffix_number, const Direction dir) {
    node_type recent_node = ambiguous_path.get_first_node();

    auto adj_nodes     = agraph.get_adj_nodes_front(recent_node);
    auto adj_nodes_end = agraph.get_adj_nodes_back (recent_node);
    std::set<path> results;
    while(adj_nodes!=adj_nodes_end){//Check if ambiguous or not
        if(*adj_nodes!=recent_node && !assigned.at(*adj_nodes) && suffix_number>0) {
            is_ambiguous(path(), recent_node, *adj_nodes, results, suffix_number, Ambiguous_Depth_Limit, dir);//Not quite right, should only count unassigned things. Did I fix this?
        }
        adj_nodes++;
    }

    auto front = ambiguous_path.get_front_iterator();
    auto back  = ambiguous_path.get_end_iterator();

    return get_unique_path(results);
}

/// Method to expand paths which are locally ambiguous and directly modifiy
/// the path. Expansion is done accoring to the defined direction.
/// \param path_to_test path that we with to try and expand further
/// \param suffix_number suffix that we are interested in
/// \param dir Is the direction to try and expand the paths
void chromotrace::manage_multiple(path& ambiguous_path, unsigned long suffix_number, const Direction dir){
    node_type recent_node = get_recent_node(ambiguous_path, dir);

    std::cout << "Start Manage Multiple "<< ambiguous_path.get_size()<< std::endl;
    bool first_left;

    try{
        first_left  = seg_to_label_map.at(ambiguous_path.get_last_node()) > seg_to_label_map.at(ambiguous_path.get_first_node());

    }catch(const std::out_of_range& e1){
        std::cerr << "manage multiple" << std::endl;
        exit(EXIT_FAILURE);
    }

    auto adj_nodes     = agraph.get_adj_nodes_front(recent_node);
    auto adj_nodes_end = agraph.get_adj_nodes_back (recent_node);

    std::cout << agraph.get_degree (*adj_nodes) << " <- Start. End -> "<< agraph.get_degree(*adj_nodes_end) << std::endl;

    std::set<path> results;
    while(adj_nodes!=adj_nodes_end){//Check if ambiguous or not
        if(*adj_nodes!=recent_node && !assigned.at(*adj_nodes) && suffix_number>0) {
            is_ambiguous(path(), recent_node, *adj_nodes, results, suffix_number, Ambiguous_Depth_Limit, dir);//Not quite right, should only count unassigned things. Did I fix this?
        }
        adj_nodes++;
    }

    auto front = ambiguous_path.get_front_iterator();
    auto back  = ambiguous_path.get_end_iterator();

    path extension  = get_unique_path(results);
    auto front_iter = extension.get_front_iterator();
    auto end_iter   = extension.get_end_iterator();

    if(dir && extension.get_size()>1){
        while (front_iter != end_iter) {
            assigned.at(*front_iter) = true;

            if(!first_left)
                ambiguous_path.add_last_node(*front_iter);
            else
                ambiguous_path.add_first_node(*front_iter);
            assert(stree.valid_pos(suffix_number));
            assert(*front_iter<=agraph.getInitialNumSegProbes());

            ++twice_assigned.at(suffix_number);

            if(twice_assigned.at(suffix_number)==2){
                next_string.at(suffix_number)     = 'A';
                next_string_rev.at(rtree.get_text_length()-suffix_number-1) = 'A';
            }

            seg_to_label_map.insert(Seg_lab_map(*(front_iter++), suffix_number--));
        }
    } else  if( extension.get_size()>1){
        while (front_iter != end_iter) {
            assigned.at(*front_iter) = true;
            if(first_left)
                ambiguous_path.add_last_node(*front_iter);
            else
                ambiguous_path.add_first_node(*front_iter);
            assert(stree.valid_pos(suffix_number));
            assert(*front_iter<=agraph.getInitialNumSegProbes());
            ++twice_assigned.at(suffix_number);

            if(twice_assigned.at(suffix_number)==2){
                next_string.at(suffix_number)     = 'A';
                next_string_rev.at(rtree.get_text_length()-suffix_number-1) = 'A';
            }

            seg_to_label_map.insert(Seg_lab_map(*(front_iter++), suffix_number++));
        }
    }

    //std::cout << "post "<< ambiguous_path.get_size()<< std::endl;

}


/// Method to deal with an ambiguous segment probe, this will recursively
/// \param path_to_test path that we with to try and expand further
/// \param recent_node Most recent node to prevent recursing on the last node we used
/// \param current_node current segment node
/// \param results reference to the set of resulting paths
/// \param suffix_number suffix that we are interested in
/// \param depth the depth of the recursion.
/// \param dir Is the direction to try and expand the paths
void chromotrace::is_ambiguous(path path_to_test, const node_type recent_node, const node_type current_node, std::set<path>& results, const unsigned long suffix_number, const unsigned long depth, const Direction dir){//TODO: Reimplement this so there is less copying
    if(depth>0 && !assigned.at(current_node)){
        auto adj_nodes     = agraph.get_adj_nodes_front(current_node);
        auto adj_nodes_end = agraph.get_adj_nodes_back (current_node);

        //std::cout << "ambiguous " << params.get_colour(current_node) << " == " << params.getLabel_sub_string(suffix_number,1)[0] << std::endl;

        if(*adj_nodes!=recent_node && (params.get_colour(current_node)==params.getLabel_sub_string(suffix_number,1).at(0)) && suffix_number>0) {
            while(adj_nodes!=adj_nodes_end){//Check if ambiguous or not
                assert(suffix_number>0);
                assert(suffix_number<stree.get_text_length());
                path temp_path(path_to_test);
                //std::cout << "Pre" << std::endl;
                if(temp_path.in_path(current_node))
                    return;
                temp_path.add_last_node(current_node);
                //std::cout << get_colour_string(temp_path) << std::endl;
                if(dir)
                    is_ambiguous(temp_path, current_node, *(adj_nodes++), results, suffix_number-1, depth-1, dir);
                else
                    is_ambiguous(temp_path, current_node, *(adj_nodes++), results, suffix_number+1, depth-1, dir);
            }
        }
    } else {
        if(path_to_test.get_size()==path_to_test.unique_node_numbers())//This requires sorting.
            results.insert(path_to_test);
    }
}

/// Find the longest unique path from a set of paths.
/// \param results The Path object that is longest unique path
/// \return The longest unique path
path chromotrace::get_unique_path(const std::set<path>& results) const {
    unsigned long result = 0;
    path position;

    std::cout << "Get unique path: " << results.size() << std::endl;

    for(auto const & current : results){
        //std::cout << "Finding unique " << get_colour_string(current) << std::endl;
        if (current.get_size() == result)//check if path is unique
            position = path();
        else if (current.get_size() > result) {//check if largest
            result   = current.get_size();
            position = path(current);
        }
    }

    std::cout << "Finding unique " << get_colour_string(position) << std::endl;

    return position;
}

/// Find the longest unique path from a set of paths.
/// \param results The Path object that is longest unique path
/// \return The longest unique path
path chromotrace::get_long_path(std::set<path>& results){
    unsigned long result = 0;
    path position;

    for(auto const & current : results){

        if (current.get_size() == result){

        }//check if path is unique//position = path();
        else if (current.get_size() > result) {//check if largest
            result   = current.get_size();
            position = path(current);
        }
    }

    //if(result>0)
    //    std::cout << "Longest Path in set is of length: " << result<< std::endl;
    return position;
}

/// For an ambiguous segment node get the number of unassigned adjacent nodes with colour col
/// \param col the colour to be counted
/// \param recent_node is the node to check
/// \return a tuple containing the number of unassigned adjacent nodes with colour col and a position with that colour
std::tuple<int, unsigned long> chromotrace::get_colour_count(const char& col, const unsigned long recent_node) const {
    auto adj_nodes     = agraph.get_adj_nodes_front(recent_node);
    auto adj_nodes_end = agraph.get_adj_nodes_back(recent_node);

    auto col_count =  0;
    unsigned long position = 0;
    while(adj_nodes!=adj_nodes_end) {//Check if ambiguous or not
        if((params.get_colour(*adj_nodes) == col) && *adj_nodes!=recent_node && !assigned.at(*adj_nodes)) {
            col_count++;
            position = *adj_nodes;
        }
        adj_nodes++;
    }
    return std::tuple<int, unsigned long>{col_count, position};
}

/// For the given path determine the colour string associated with it
/// \param current_path is a reference to a path object
/// \return the colour string associated with current_path
std::string chromotrace::get_colour_string(const path& current_path) const {
    auto iterator       = current_path.get_front_iterator();
    std::string colour_string;
    for(auto path_index = 0; path_index < current_path.get_size() ; path_index++){//Get colour string
        colour_string.append(1, params.get_colour(*(iterator++)));
    }
    colour_string.append("#");//Unique terminating symbol to avoid \0 matching confusing unique matches

    return colour_string;//Is this moving?
}

/// Free paths that had been allocated but could not be found
/// in the labelling data
/// \param current_path is the path to the freed
void chromotrace::free_unlocated_path(path& current_path){
    auto  iter  = current_path.get_front_iterator();
    while(iter != current_path.get_end_iterator()){
        assigned.at(*(iter++)) = false;
    }
}


/// Get the node in the path that is closest to the start or end of the string depending on the Direction specified. Reverse = start of string, Forward = end of string.
/// \param ambiguous_path is the path we're interested in
/// \param dir the Direction we want the node for
/// \return The node closest to the start or end of the string depending on the Direction.
unsigned long chromotrace::get_recent_node(const path& ambiguous_path, const Direction dir) const {
    try{
        if(dir) {
            return (seg_to_label_map.at(ambiguous_path.get_last_node()) > seg_to_label_map.at(ambiguous_path.get_first_node())) ? ambiguous_path.get_first_node() : ambiguous_path.get_last_node();//Find Left point
        } else {
            return (seg_to_label_map.at(ambiguous_path.get_last_node()) < seg_to_label_map.at(ambiguous_path.get_first_node())) ? ambiguous_path.get_first_node() : ambiguous_path.get_last_node();//Find Right point
        }
    }catch(const std::out_of_range &e1){
        std::cerr << "get recent node " << e1.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

/// Check if a node has been resolved as part of a path 
/// \param recent_node  the node to check if it is part of a path
/// \param dir the direction we want to check in
/// \return if the node has been resolved
bool chromotrace::check_if_resolved(unsigned long recent_node, Direction dir) const {
    auto adj_nodes        = agraph.get_adj_nodes_front(recent_node);
    auto adj_nodes_end    = agraph.get_adj_nodes_back (recent_node);

    auto suffix = seg_to_label_map.at(recent_node);

    while(adj_nodes!=adj_nodes_end){//Check if ambiguous or not
        if(assigned.at(*adj_nodes)) {
            if (dir) {//Reverse
                if (seg_to_label_map.at(*adj_nodes) == suffix - 1) {
                    return true;
                }
            } else {//Forward
                if (seg_to_label_map.at(*adj_nodes) == suffix + 1) {
                    return true;
                }
            }
        }
        adj_nodes++;
    }
    return false;
}

/// Write the output of the algorithm to the
/// specified output file from params.get_output_file()
///
void chromotrace::write_output() const{
    std::cout << "Start Writing output" << std::endl;
    std::ofstream output_file (params.get_output_file());

    if(output_file.is_open()) {
        output_file << "chrom\tstart\tend\tcolour\tx\ty\tz" << std::endl;

        for (const auto& probe_mapping : seg_to_label_map) {
            if(probe_mapping.second<stree.get_text_length()){
                output_file << params.get_chromo(probe_mapping.second)    << "\t";
                output_file << params.get_start_pos(probe_mapping.second) << "\t";
                output_file << params.get_end_pos(probe_mapping.second)   << "\t";
                output_file << params.get_colour(probe_mapping.first)     << "\t";
                output_file << params.get_x(probe_mapping.first) << "\t";
                output_file << params.get_y(probe_mapping.first) << "\t";
                output_file << params.get_z(probe_mapping.first) << "\t";
                output_file << std::endl;
            } else {
            }
        }
    } else {
        std::cout << "File could not opened" << std::endl;
        std::cout << "Make sure you have write permissions or that the file doesn't already exist" << std::endl;
    }

    std::cout << "Output Complete "<< std::endl;
}

/// Initialise the chromotrace algorithm and repeat method until it doesn't find any additional probes
/// \return an error code
int chromotrace::find_paths() {

        auto total_probe_count = 0;
        auto old_total_probe_count = 1;
        while(total_probe_count != old_total_probe_count) {
            stree = suffix_tree(next_string);//Uses new suffix tree class. Doesn't store suffix
            std::cout << next_string<< std::endl;
            rtree = suffix_tree(next_string_rev);//Uses new suffix tree class. Doesn't store suffix trees

            old_total_probe_count = total_probe_count;

            initialize_paths();
            std::cout << "Initialised paths" << std::endl;

            identify_position();
            std::cout << "Identified Positions" << std::endl;
            std::cout << "Paths " <<  paths_to_extend.size() << " " << path_vector.size() <<  std::endl;

            remove_assigned_nodes();

            bool continue_loop = true;

            while (continue_loop) {
                std::cout << "Continue Loop " << paths_to_extend.size() << " " << path_vector.size()  << std::endl;
                continue_loop   = false;
                auto front_iter = paths_to_extend.begin();
                auto end_iter   = paths_to_extend.end();

                std::list<Path_to_extend_lookup> paths_to_extend_temp;
                std::cout << "Paths to extend loop" << std::endl;
                while (front_iter != end_iter) {//TODO: If we can do it quickly then only add those paths that may be changed by an update and don't loop through the entire path set each time. Is this premature optimisation?
                    auto pre_size = path_vector.at(std::get<0>(*front_iter)).get_size();
                    auto suffix = expand_path(std::get<0>(*front_iter), std::get<1>(*front_iter), std::get<2>(*front_iter));
                    auto post_size = path_vector.at(std::get<0>(*front_iter)).get_size();
                    if (pre_size < post_size)
                        continue_loop = true;
                    std::cout << "mid paths to extend loop" << std::endl;
                    if (stree.valid_pos(suffix)) {
                        paths_to_extend_temp.emplace_back(Path_to_extend_lookup(std::get<0>(*front_iter), suffix, std::get<2>(
                                *front_iter)));//Get last suffix for middle part of tuple
                        assert(suffix > 0);
                        assert(suffix < stree.get_text_length());
                    }

                    std::cout << "after if paths to extend loop" << std::endl;
                    front_iter++;

                }

                paths_to_extend = paths_to_extend_temp;
                std::cout << "End paths to extend loop" << std::endl;
            }

            total_probe_count = 0;
            for(auto const & cur_pos : assigned){
                total_probe_count += cur_pos;
            }
            std::cout <<"Condition " <<total_probe_count << " " << old_total_probe_count << " " << seg_to_label_map.size() << std::endl;
            assert(total_probe_count==seg_to_label_map.size());
        }

    auto total_probe_count_3 = 0;
    for(auto const & cur_pos : assigned){

        total_probe_count_3 += cur_pos;
    }


    std::cout << std::endl;
    std::cout << "Total Number of Probes Located: "          << total_probe_count_3 << std::endl;
    std::cout << "Out of: "   << agraph.getInitialNumSegProbes() << std::endl;

    write_output();
    return 0;
}
