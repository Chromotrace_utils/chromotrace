///
/// This class loads the parameters and processes them
/// to put them in a useful form for the rest of the program
/// and provide the appropriate methods to safely access them.
///
#ifndef CHROMOTRACE_NEW_PARAMETERS_H
#define CHROMOTRACE_NEW_PARAMETERS_H

#include <string>
#include <memory>
#include <vector>

class parameters {
private:
    float  threshold;
    std::string output_file;

    /** START: Variables to store information in labelling file **/
    std::string label_string;
    std::vector<std::string>   chromosome;
    std::vector<unsigned long> start_pos;
    std::vector<unsigned long> end_pos;

    unsigned long num_probes;
    /**END**/

    /** START: Variables to store information in segment file **/
    std::vector<long double> x;
    std::vector<long double> y;
    std::vector<long double> z;
    std::vector<char> colour;
    /**END**/

    std::vector<char> alpha = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','#'};

public:
    /// Primary constructor for building a parameters object from a configuration file
    /// \param configfile the configuration file the parameters object should be built from
    explicit parameters(std::string &configfile) {
        read_params(configfile);
    }

    parameters(std::string &configfile, std::string &output_location, float new_threshold) {
        read_params(configfile, output_location, new_threshold);
    }


    void read_params(std::string &filename);
    void read_params(std::string &filename, std::string &output_location, float new_threshold);

    /**START: Generic get methods for important data**/
    const std::string &getLabel_string() const;
    const std::string getLabel_string_reverse() const;
    const std::string getLabel_sub_string(unsigned long start, unsigned long end) const;

    float get_threshold() const;
    unsigned long get_num_probes() const;
    /**END**/

    /**START: Variables to access labelling file**/
    unsigned long get_start_pos(unsigned long) const;
    unsigned long get_end_pos(unsigned long)   const;
    std::string   get_chromo(unsigned long)    const;
    /**END**/

    /**START: Variables to access segment file**/
    long double get_x(unsigned long) const;
    long double get_y(unsigned long) const;
    long double get_z(unsigned long) const;
    char get_colour(unsigned long)   const;

    std::vector<long double> get_x_vec() const;
    std::vector<long double> get_y_vec() const;
    std::vector<long double> get_z_vec() const;
    /**END**/

    std::string get_output_file() const;

private:
    /**Methods to process input files**/
    void process_lab_file(std::string &label_file);
    void process_2_col_lab_file(std::string &label_file);
    void process_seg_file(std::string &seg_file);
};


#endif //CHROMOTRACE_NEW_PARAMETERS_H
