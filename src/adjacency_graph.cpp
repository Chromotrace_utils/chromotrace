//
// Created by carl on 10/10/16.
//

#include "adjacency_graph.h"
#include <cmath>
#include <algorithm>
#include <assert.h>

/// remove node from the graph and update
/// the appropriate data structures.
/// \param node_index is a valid index of a node
void adjacency_graph::remove_node(const unsigned int node_index){

    if(degree_array.at(node_index)!=0){//Check if it's already been deleted or doesn't need to be anyway

        std::cout << "Removing node: " << node_index << std::endl;
        auto front_iter = get_adj_nodes_front(node_index);
        auto back_iter  = get_adj_nodes_back (node_index);

        while(front_iter!=back_iter){
            degree_array.at(*front_iter)--;//decrease degree
            adj_nodes.at(*front_iter).remove(node_index);//remove from adj nodes
            front_iter++;
        }

        adj_nodes.at(node_index) = std::list<unsigned long>();//clear from adj_nodes
        degree_array.at(node_index) = 0;//0 degree
    }

    --num_seg_probes;
}

/// Get degrees of each adj node at index node_index
/// \param node_index is the index of a node
/// \return degree of node node_index
std::list<unsigned long> adjacency_graph::get_degree_adj_nodes(const unsigned int node_index) const{
        return adj_nodes.at(node_index);
}

/// Get degree of node at index node_index
/// \param node_index is the index of a node
/// \return degree of node node_index
unsigned int adjacency_graph::get_degree(const unsigned int node_index) const{
    return degree_array.at(node_index);
}

/// get adjacency list of node at index node_index
/// \param node_index is the index of a node
/// \return adjacency list for node node_index
std::list<unsigned long>::const_iterator adjacency_graph::get_adj_nodes_front(const unsigned int node_index) const{
    return adj_nodes.at(node_index).begin();
}

/// get adjacency list of node at index node_index
/// \param node_index is the index of a node
/// \return adjacency list for node node_index
std::list<unsigned long>::const_iterator adjacency_graph::get_adj_nodes_back(const unsigned int node_index) const{
    return adj_nodes.at(node_index).end();
}

/// Return the number of nodes in the distance matrix
/// \return integer value representing the number of nodes in the distance matrix
unsigned long adjacency_graph::get_num_seg_probes() const{
    return num_seg_probes;
}

/// Getter for distance matirix so distance matrix cannot be modified
/// \param x is an index of a node
/// \param y is an index of a node
/// \return 1 if node x and y are at distance < threshold, 0 otherwise
/*bool adjacency_graph::get_element(const unsigned int x, const unsigned int y) const{
    if (x>y)
        return distance_matrix.at(x)(y);
    else
        return distance_matrix.at(y)(x);
}*/

/// Compute the euclidean distance between 2 points passed as r-value ref
/// \param a is a Point
/// \param b is a Point
/// \return is the euclidean distance between a and b
inline float adjacency_graph::compute_euclidean_distance(const Point&& a, const Point&& b) const{
    float distance=0;

    distance += (a.x-b.x)*(a.x-b.x);
    distance += (a.y-b.y)*(a.y-b.y);
    distance += (a.z-b.z)*(a.z-b.z);

    return(std::sqrt(distance));
}

/// Compute the euclidean distance between 2 points passed as ref
/// \param a is a Point
/// \param b is a Point
/// \return is the euclidean distance between a and b
inline float adjacency_graph::compute_euclidean_distance(const Point& a, const Point& b) const{
    float distance=0;

    distance += (a.x-b.x)*(a.x-b.x);
    distance += (a.y-b.y)*(a.y-b.y);
    distance += (a.z-b.z)*(a.z-b.z);

    return(std::sqrt(distance));
}

/// Start to compute the euclidean distance between 2 points passed as r-value ref but don't square root the result
/// \param a is a Point
/// \param b is a Point
/// \return is the square of the euclidean distance between a and b
inline float adjacency_graph::compute_euclidean_distance_no_sqrt(const Point&& a, const Point&& b) const{
    long double distance=0;

    distance += (a.x-b.x)*(a.x-b.x);
    distance += (a.y-b.y)*(a.y-b.y);
    distance += (a.z-b.z)*(a.z-b.z);

    return(distance);
}

/// Start to compute the euclidean distance between 2 points passed as ref but don't square root the result
/// \param a is a Point
/// \param b is a Point
/// \return is the square of the euclidean distance between a and b
inline float adjacency_graph::compute_euclidean_distance_no_sqrt(const Point& a, const Point& b) const{
    long double distance=0;

    distance += (a.x-b.x)*(a.x-b.x);
    distance += (a.y-b.y)*(a.y-b.y);
    distance += (a.z-b.z)*(a.z-b.z);

    return(distance);
}

/// Compute the manhatten distance between 2 points passed as r-value ref
/// \param a is a Point
/// \param b is a Point
/// \return is the euclidean distance between a and b
inline float adjacency_graph::compute_manhatten_distance(const Point&& a, const Point&& b) const{
    long double dist=0;
    dist += (a.x-b.x);
    dist += (a.y-b.y);
    dist += (a.z-b.z);
    return (std::abs(dist));
}

/// Compute the manhatten distance between 2 points passed as ref
/// \param a is a Point
/// \param b is a Point
/// \return is the euclidean distance between a and b
inline float adjacency_graph::compute_manhatten_distance(const Point& a, const Point& b) const {
    long double dist = 0;
    dist += (a.x - b.x);
    dist += (a.y - b.y);
    dist += (a.z - b.z);
    return (std::abs(dist));
}

/// Initialise and build the distance matrix and degree array
/// \param x vector of double precision x co-ordinates that the matrix should be built from
/// \param y vector of double precision y co-ordinates that the matrix should be built from
/// \param z vector of double precision z co-ordinates that the matrix should be built from
/// \param threshold_dis float representing the threshold under which points will be considered adjacent
void adjacency_graph::build_distance_matrix(std::vector<long double> x, std::vector<long double> y, std::vector<long double> z, float threshold_dis){
    //TODO: Create a number of different approaches so that all methods work but representation can be matrix, list or both
    //TODO: Sort the points of one list so I can terminate the loops early depending on the distance parameter. x, y and z must all be in the same order
    std::cout << "Starting build with threshold " << threshold_dis << std::endl;


    //float threshold = threshold_dis*threshold_dis;
    unsigned long number_of_probes = get_num_seg_probes();

    if(number_of_probes==0){
        std::cerr << "ERROR: 0 probes found, cannot build graph. Check probe file and file path" << std::endl;
        exit(1);
    }

    std::vector<unsigned long> indexes(number_of_probes, 0);
    //std::vector<unsigned long> inverted_indexes(number_of_probes, 0);

    degree_array = std::vector<unsigned int>(number_of_probes);//Not deleted
    adj_nodes    = std::vector<std::list<unsigned long>>(number_of_probes);

    for(auto i = 0; i<number_of_probes; ++i) {degree_array.at(i) = 0;}

    std::cout << "Filling in matrix with " << number_of_probes << " probes." << std::endl;

    /* Compute adjacency matrix and degree array*/
    for(unsigned long x_axis = 0; x_axis < number_of_probes; ++x_axis){//Compute adjacent nodes
        for(unsigned long y_axis = (x_axis+1); y_axis < number_of_probes; ++y_axis){
            //if((x.at(y_axis) - x.at(x_axis))>threshold_dis){break;}
            auto dist = compute_euclidean_distance(Point(x.at(x_axis),y.at(x_axis),z.at(x_axis)), Point(x.at(y_axis),y.at(y_axis),z.at(y_axis))) <=threshold_dis;
            //distance_matrix[x_axis][y_axis] = dist;
            degree_array.at(x_axis) += dist;
            degree_array.at(y_axis) += dist;
            if(dist){adj_nodes.at(x_axis).push_back(y_axis);adj_nodes.at(y_axis).push_back(x_axis);}
        }
    }

    std::cout << "Matrix Completed" << std::endl;

}

int adjacency_graph::getMinDegree() const {
    return min_degree;
}

int adjacency_graph::getMaxDegree() const {
    return max_degree;
}

unsigned long adjacency_graph::getInitialNumSegProbes() const {
    return initial_num_seg_probes;
}
